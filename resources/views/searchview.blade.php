<table class="table">
    <thead class="thead-light">
      <tr>
        <th>Donut Location</th>
        <th>Coordinates</th>
        <th>GG location</th>
        <th>Haversine</th>
        <th>Location Type</th>
        <th>Match Type</th>
        <th>Options</th>
      </tr>
    </thead>
    <tbody>
    @foreach($locations as $location)

      <tr class="remove-{{$location->raw_id}}">
        <td>{{$location->indeed_location_name}}</td>
        <td>{{$location->indeed_coordinates}}</td>
        <td>{{(is_numeric($location->google_location_name) ) ? $location->formatted_address : $location->google_location_name}}</td>
        <td>{{$location->gg_equivalent_haversine_distance}}</td>
        <td>{{locationType($location)}}</td>
        <td>{{$location->explanation_type}}</td>
        <td><a data-toggle="collapse" href="#{{$location->raw_id}}" role="button" aria-expanded="false" aria-controls="collapseExample">Details</a> |
        <a href="#" class="remove-btn" data-id="{{$location->raw_id}}"><i class="fas fa-trash-alt"></i></a> |
        <a href="#" class="undecided-btn"><i class="fas fa-question-circle pop" data-id="{{$location->raw_id}}" data-container="body" data-toggle="popover" data-placement="top" data-content="Move the specific location to undecided?"></i></a>
        </td>
      </tr>
      <tr class="collapse remove-{{$location->raw_id}}" id="{{$location->raw_id}}">
        <td colspan="7">
          <div class="card card-body">
            <div class="row">

            {{-- Start Indeed Details --}}
              <div class="col">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title text-primary">Indeed Location Details</h5>
                  </div>
                  <div class="card-body">

                    <ul class="list-unstyled">
                      <li>name: <span class="text-primary">{{$location->indeed_location_name}}</span></li>
                      <li>coordinates: <span class="text-primary">{{$location->indeed_coordinates}}</span></li>
                      <li>state: <span class="text-primary">{{$location->state}}</span></li>
                      <li>region: <span class="text-primary">{{$location->region}}</span></li>
                      <li>formatted name: <span class="text-primary">{{$location->formatted_location_full}}</span></li>
                      <li>postal code: <span class="text-primary">{{(substr_count($location->formatted_location_full, " ") ) ? explode(" ",$location->formatted_location_full)[0] : ""}}</span></li>
                      <li>job count: <span class="text-primary">{{$location->job_count}}</span></li>
                    </ul>
                  </div>
                </div>
              </div>

            {{-- Start Google Details --}}
              <div class="col">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title text-danger">Google Location Details</h5>
                  </div>
                  <div class="card-body">
                    <ul class="list-unstyled">
                      <li>name: <span class="text-danger">{{$location->google_location_name}}</span></li>
                      <li>coordinates: <span class="text-danger">{{(float)$location->google_location_lat}}, {{(float)$location->google_location_lng}}</span></li>
                      <li>
                        administrative area:
                        <ul>
                          <li>level 1: <span class="text-danger">{{$location->google_admin_area_level_1_long}}</span></li>
                          <li>level 2: <span class="text-danger">{{$location->google_admin_area_level_2_long}}</span></li>
                          <li>level 3: <span class="text-danger">{{$location->google_admin_area_level_3_long}}</span></li>
                        </ul>
                      </li>
                      <li>locality: <span class="text-danger">{{$location->google_locality_long}}</span> </li>
                      <li>
                        sublocality
                        <ul>
                          <li>level 1: <span class="text-danger">{{$location->google_sublocality_level_1_long}}</span></li>
                          <li>level 2: <span class="text-danger">{{$location->google_sublocality_level_2_long}}</span></li>
                        </ul>
                      </li>
                      <li>postal code: <span class="text-danger">{{$location->google_postal_code}}</span></li>
                      <li>formatted name: <span class="text-danger">{{$location->formatted_address}}</span></li>
                      <li>location type: <span class="text-danger">{{locationType($location)}}</span></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>
    @endforeach

    </tbody>
    </table>
    {!!$locations->links("pagination::bootstrap-4")!!}
