@extends('layout.master')
@section('content')
<br>
<br>
<div class="row">
	<div class="container"></div>
</div>
<div class="container">
	<div class="row">
		<table class="table">
			<thead>
				<tr>
					<th>Query</th>
					<th>Error</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
				@foreach($logs as $log)
				<tr>
					<td>{{$log->query}}</td>
					<td>{{$log->error}}</td>
					<td>{{explode(' ',$log->created_at)[0]}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{{$logs->links("pagination::bootstrap-4")}}
</div>
@endsection
