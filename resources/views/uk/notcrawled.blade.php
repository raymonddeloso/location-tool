@extends('layout.master')

@section('content')
	<div class="container" id="app">
		<div class="row">
			<div class="col">
				<h3 class="mb-5 mt-5 text-center">Not Crawled UK Locations <span class="text-primary" id="explained-count">@{{count}}</span></h3>
			</div>
		</div>
		<div class="row">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Donut Location</th>
			            <th>Coordinates</th>
			            <th>State</th>
			            <th>Donut Loc. Full</th>
			            <th>Job Count</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="(location,key) in uncrawledLocations">
						<td><a href="#" >@{{location.indeed_location}}</a></td>
			            <td>@{{location.indeed_lat}},@{{location.indeed_lon}}</td>
			            <td>@{{location.state}}</td>
			            <td>@{{location.formatted_location_full}}</td>
			            <td>@{{location.job_count}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
@endsection

@push('js')
<script src="{{mix('js/uk_not_crawled.js')}}"></script>
@endpush
