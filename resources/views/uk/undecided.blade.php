@extends('layout.master')


@section('content')
	<div class="container" id="app">

	<div class="row">
			<div class="col">
				<h3 class="mb-3 mt-3 text-center">Undecided UK Locations <span class="text-primary" id="explained-count">@{{count}}</span></h3>
			</div>
		</div>

		<div class="row">
			<table class="table">
				<thead class="thead-light">
					<tr>
						<th>Donut Loc</th>
			            <th>Coordinates</th>
			            <th>State</th>
			            <th>Donut Loc. Full</th>
			            <th>Job Count</th>
					</tr>
				</thead>
				<tbody>
					<template v-for="(location,key) in undecidedLocations">
						<tr>
							<td><a href="#" @click.prevent="getDetails(location)">@{{location.indeed_location_name}}</a></td>
							<td>@{{location.indeed_coordinates}}</td>
							<td>@{{location.state}}</td>
							<td>@{{location.formatted_location_full}}</td>
							<td>@{{location.job_count}}</td>
						</tr>
						<tr v-if="selectedLocation.hasOwnProperty('id') && selectedLocation.id == location.id">
							<td colspan="5">
								<div class="row">
									<div class="col-sm-3">
										<div class="card">
					                      <div class="card-header">
					                        <h5 class="card-title text-primary">Donut Location Details</h5>
					                      </div>
					                      <div class="card-body">

					                        <ul class="list-unstyled">
					                          <li>name: <span class="text-primary">@{{selectedLocation.indeed_location_name}}</span></li>
					                          <li>coordinates: <span class="text-primary">@{{selectedLocation.indeed_coordinates}}</span></li>
					                          <li>state: <span class="text-primary">@{{selectedLocation.state}}</span></li>
					                          <li>region: <span class="text-primary">@{{selectedLocation.region}}</span></li>
					                          <li>formatted name: <span class="text-primary">@{{selectedLocation.formatted_location_full}}</span></li>
					                          <li>postal code: <span class="text-primary"></span></li>
					                          <li>job count: <span class="text-primary">@{{selectedLocation.job_count}}</span></li>
					                          <hr>
					                          <li>Sample Job Url's:
												{{--<ul class="list-unstyled">
													@foreach($location->hasManyUrl as $url)
														<li style="font-size: 14px" class="text-primary">{{$url->url}}</li>
													@endforeach
												</ul>--}}
					                          </li>
					                        </ul>
					                      </div>
					                    </div>
									</div>
									<template v-for="chunk in googleLocations">
										<div class="col-sm-3" v-for="gloc in chunk">
						                    <div class="card" >
						                      <div class="card-header">
						                        <h5 class="card-title text-danger">Google Location Details</h5>
						                      </div>
						                      <div class="card-body">
						                        <ul class="list-unstyled">
						                          <li>name: <span class="text-danger">@{{gloc.google_location_name}}</span></li>
						                          <li>coordinates: <span class="text-danger"></span></li>
						                          <li>
						                            administrative area:
						                            <ul>
						                              <li>level 1: <span class="text-danger">@{{gloc.google_admin_area_level_1_long}}</span></li>
						                              <li>level 2: <span class="text-danger">@{{gloc.google_admin_area_level_2_long}}</span></li>
						                              <li>level 3: <span class="text-danger">@{{gloc.google_admin_area_level_3_long}}</span></li>
						                            </ul>
						                          </li>
						                          <li>locality: <span class="text-danger">@{{gloc.google_locality_long}}</span> </li>
						                          <li>
						                            sublocality
						                            <ul>
						                              <li>level 1: <span class="text-danger">@{{gloc.google_sublocality_level_1_long}}</span></li>
						                              <li>level 2: <span class="text-danger">@{{gloc.google_sublocality_level_2_long}}</span></li>
						                            </ul>
						                          </li>
						                          <li>postal code: <span class="text-danger">@{{gloc.google_postal_code}}</span></li>
						                          <li>formatted name: <span class="text-danger">@{{gloc.formatted_address}}</span></li>
						                          <li>location type: <span class="text-danger">@{{locationType(gloc)}}</span></li>
						                          <hr>
						                          <li>haversine: <span class="text-danger">@{{haversince(gloc.google_location_lat,gloc.google_location_lng,selectedLocation.indeed_coordinates)}}</span></li>
						                        </ul>
						                      </div>
						                      <div class="card-footer">
						                        <div class="row">
						                        	<div class="col">
							                        	<button class="btn btn-sm btn-primary btn-block select-btn" @click="assign(gloc)">Select</button>
							                        </div>
						                        </div>
						                      </div>
						                    </div>
					                  	</div>
									</template>
								</div>
							</td>
						</tr>
					</template>
				</tbody>
			</table>
		</div>
	</div>
@endsection

@push('js')
	<script src="{{mix('js/uk_undecided.js')}}"></script>
@endpush
