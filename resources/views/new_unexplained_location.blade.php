@extends('layout.master')
@push('css')
<style>
	#center-container{
			position: absolute;
			right:10px;
			width: 40%;
			min-height:100px;
			max-height: 800px;
			overflow-y: auto;
			background-color: white;
			z-index: 10;
			border:3px solid #e6a822;
			border-radius: 5px;
		}
		#center-containerheader {
			z-index: 10;
		}
</style>
@endpush
@section('content')
<div id="app">
	<div style="background-color:white;overflow-y:scroll;padding:10px;width: 75%;height: 100vh;right:0;position: absolute;z-index: 5" v-if="visibleGoogleAddresses">
		<div class="row">
			<div class="container-fluid">
				<button type="button" class="close" aria-label="Close" v-on:click="closeGoogleAddresses">
				  <span aria-hidden="true" class="text-danger">&times;</span>
				</button>
			</div>
		</div>
		<div class="row" v-for="chunk in googleAddresses">

					<div class="col-4" v-for="google in chunk">
						<div class="card mb-3">
							<div class="card-header">
							   <h5 class="card-title text-danger">Google Location Details</h5>
							</div>
						  	<div class="card-body">
							    <ul class="list-unstyled">
							      <li>name: <span class="text-danger">@{{google.google_location_name}}</span></li>
							      <li>coordinates: <a v-bind:href="'https://www.google.com/maps/dir/?api=1&origin='+selectedLocation.indeed_coordinates+'&destination='+parseFloat(google.google_location_lat)+','+parseFloat(google.google_location_lng)" target="_blank">@{{parseFloat(google.google_location_lat)}},@{{parseFloat(google.google_location_lng)}}</a></li>
							      <li>
							        administrative area:
							        <ul>
							          <li>level 1: <span class="text-danger">@{{google.google_admin_area_level_1_long}}</span></li>
							          <li>level 2: <span class="text-danger">@{{google.google_admin_area_level_2_long}}</span></li>
							          <li>level 3: <span class="text-danger">@{{google.google_admin_area_level_3_long}}</span></li>
							        </ul>
							      </li>
							      <li>locality: <span class="text-danger">@{{google.google_locality_long}}</span> </li>
							      <li>
							        sublocality
							        <ul>
							          <li>level 1: <span class="text-danger">@{{google.google_sublocality_level_1_long}}</span></li>
							          <li>level 2: <span class="text-danger">@{{google.google_sublocality_level_2_long}}</span></li>
							        </ul>
							      </li>
							      <li>postal code: <span class="text-danger">@{{google.google_postal_code}}</span></li>
							      <li>formatted name: <span class="text-danger">@{{google.formatted_address}}</span></li>
							      <li>location type: <span class="text-danger">@{{locationType(google)}}</span></li>
							      <hr>
							      <li>haversine: <span class="text-danger">@{{haversince(parseFloat(google.google_location_lat), parseFloat(google.google_location_lng), selectedLocation.indeed_coordinates)}}</span></li>
							    </ul>
							</div>
						  	<div class="card-footer">
							    <div class="row">
							    	<div class="col">
							        	<button class="btn btn-sm btn-block btn-primary" v-on:click="save(google.id)">Assign</button>
							        </div>
							    </div>
						  	</div>
						</div>
					</div>

		</div>
	</div>
	<div class="container-fluid">
			<div id="center-container" v-bind:class="{'d-none' : jobTitle.length == 0}">
				<div class="card">
					<div class="card-header bg-primary">
						@{{jobKey}}, @{{company}}
						<button type="button" class="close" aria-label="Close" v-on:click="closeJobContainer">
						  <span aria-hidden="true" class="text-danger">&times;</span>
						</button>
					</div>
					<div id="center-containerheader" class="card-body" style="cursor:move;max-height:743px;overflow-y: auto;">

						<h5>@{{jobTitle}}</h5>
						<div v-html="jobDescription"></div>
					</div>
				</div>
			</div>
		<div class="row">
			<div class="col-3" >
				    <div class="card" v-if="selectedLocation.length != 0">
				      <div class="card-header">
				        <h5 class="card-title text-primary">Donut Location Details</h5>
				      </div>
				      <div class="card-body">

				        <ul class="list-unstyled">
				          <li>name: <span class="text-primary">@{{selectedLocation.indeed_location_name}}</span></li>
				          <li>coordinates: <a target="_blank" v-bind:href="'https://www.google.com/maps/search/?api=1&query='+selectedLocation.indeed_coordinates">@{{selectedLocation.indeed_coordinates}}</a></li>
				          <li>state: <span class="text-primary">@{{selectedLocation.state}}</span></li>
				          <li>region: <span class="text-primary">@{{selectedLocation.region}}</span></li>
				          <li>formatted name: <span class="text-primary">@{{selectedLocation.formatted_location_full}}</span></li>
				          <li>postal code: <span class="text-primary">@{{selectedLocation.zip_code}}</span></li>
				          <li>job count: <span class="text-primary">@{{selectedLocation.job_count}}</span></li>
				          <li>Sample Job Url's: <span><a href="#" v-on:click="getJobKeys(selectedLocation.indeed_location_name,selectedLocation.indeed_coordinates)">@{{viewJobKeyText}}</a></span>
							<ul  style="max-height: 450px;overflow-x: auto">
									<li v-for="(jobkey,key) in jobKeys"><a href="#" style="cursor: pointer;" v-on:click.prevent="clickJobKey(key)">@{{jobkey.jobkey}}</a></li>
							</ul>
				          </li>
				        </ul>
				      </div>
				      <div class="card-footer">
				      	<div class="row">
				      		<div class="col">
				            	<button class="btn btn-sm btn-warning btn-block undecided-btn" v-on:click="undecided">Undecided</button>
				            </div>
				      	</div>
				      </div>
				    </div>
			</div>
			<div class="col-9">
				<div class="form-group mt-3">
					<input type="text" placeholder="Search Location" class="form-control">
				</div>
				<table class="table table-sm table-striped">
					<thead>
						<tr>
							<th>Donut Loc.</th>
							<th>Coordinates</th>
							<th>State</th>
							<th>Donut Loc. Full</th>
							<th>Job Count</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(location, key) in locations" class="" v-bind:class="{'table-primary' : (location.id == selectedId)}">
							<td><a href="#" v-on:click.prevent="getDetails(key)">@{{location.indeed_location_name}}</a></td>
							<td>@{{location.indeed_coordinates}}</td>
							<td>@{{location.state}}</td>
							<td>@{{location.formatted_location_full}}</td>
							<td>@{{location.job_count}}</td>
						</tr>
					</tbody>
				</table>

				<ul class="pagination justify-content-center">
					<li class="page-item">
						<span class="page-link">@{{total}} locations</span>
					</li>
				    <li class="page-item">
				      <a class="page-link" href="#" tabindex="-1" v-bind:class="{'disabled' :(prev_page_url == null)}" v-on:click.prevent="paginate('prev_page_url')"><<< Previous</a>
				    </li>
				    <li class="page-item" >
				      <a class="page-link" href="#" v-bind:class="{'disabled' :(next_page_url == null)}" v-on:click.prevent="paginate('next_page_url')">Next >>></a>
				    </li>
				</ul>
			</div>
		</div>

	</div>
</div>
@endsection

@push('js')
<script src="{{mix('js/new_unexplained_location.js')}}"></script>
@endpush
