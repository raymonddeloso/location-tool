@extends('layout.master')
@section('content')
	<div class="container">
		<table class="table tables-striped">
			<thead>
				<tr>
					<th>Job Count</th>
					<th>Donut Name</th>
					<th>Donut Coordinates</th>
					<th>Google Name</th>
					<th>Sub Locality lvl 1</th>
					<th>Sub Locality lvl 2</th>
					<th>Locality</th>
					<th>Haversine</th>
				</tr>
			</thead>
			<tbody>
				@foreach($locs as $loc)
				<tr>
					<td>{{$loc->job_count}}</td>
					<td>{{$loc->indeed_location_name}}</td>
					<td>{{$loc->indeed_coordinates}}</td>
					<td>{{$loc->google_location_name}}</td>
					<td>{{$loc->google_sublocality_level_1_long}}</td>
					<td>{{$loc->google_sublocality_level_2_long}}</td>
					<td>{{$loc->google_locality_long}}</td>
					<td>{{calculateHaversine($loc->indeed_coordinates,(float)$loc->google_location_lat,(float)$loc->google_location_lng)}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{{$locs->links("pagination::bootstrap-4")}}
	</div>
@endsection
