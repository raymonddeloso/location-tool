@extends('layout.master')
@section('content')
<div class="container" id="app">
  <div class="row">
    <div class="col-12">
    <br>
    <br>
    <h3>Explained Locations <span class="text-primary" id="explained-count">@{{count}}</span></h3>
    <br>
      <div class="row">
        <div class="col">
          <input type="text" class="form-control" placeholder="Search" id="searchfield" v-model="searchTerm" @input="search">
        </div>
      </div>
      <br>
      <div id="content-view">
        <table class="table">
        <thead class="thead-light">
          <tr>
            <th>Donut Location</th>
            <th>Coordinates</th>
            <th>GG location</th>
            <th>Haversine</th>
            <th>Location Type</th>
            <th>Match Type</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          <template  v-for="(location,key) in explainedLocations">
            <tr>
              <td>@{{location.indeed_location_name}}</td>
              <td>@{{location.indeed_coordinates}}</td>
              <td>@{{locationType(location)}}</td>
              <td>@{{location.gg_equivalent_haversine_distance}}</td>
              <td>@{{locationType(location)}}</td>
              <td>@{{location.explanation_type}}</td>
              <td><a href="#" @click.prevent="viewDetails(key)">Details</a> |
              <a href="#" @click.prevent="moveToUnexplained(location)"><i class="fas fa-trash-alt"></i></a> |
              <a href="#" @click.prevent="moveToUndecided(location)"><i class="fas fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Move the specific location to undecided?"></i></a>
              </td>
            </tr>
            <tr v-if="key === selectedKey">
                <td colspan="7">
                  <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                              <h5 class="card-title text-primary">
                                Donut Location Details
                              </h5>
                            </div>
                            <div class="card-body">
                              <ul class="list-unstyled">
                                <li>name: <span class="text-primary">@{{location.indeed_location_name}}</span></li>
                                <li>coordinates: <span class="text-primary">@{{location.indeed_coordinates}}</span></li>
                                <li>state: <span class="text-primary">@{{location.state}}</span></li>
                                <li>region: <span class="text-primary">@{{location.region}}</span></li>
                                <li>formatted name: <span class="text-primary">@{{location.formatted_location_full}}</span></li>
                                <li>postal code: <span class="text-primary">@{{showDonutPostalCode(location)}}</span></li>
                                <li>job count: <span class="text-primary">@{{location.job_count}}</span></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                          <div class="card-header">
                            <h5 class="card-title text-danger">Google Location Details</h5>
                          </div>
                          <div class="card-body">
                            <ul class="list-unstyled">
                              <li>name: <span class="text-danger">@{{location.google_location_name}}</span></li>
                              <li>coordinates: <span class="text-danger">@{{parseFloat(location.google_location_lat)}}, @{{parseFloat(location.google_location_lng)}}</span></li>
                              <li>
                                administrative area:
                                <ul>
                                  <li>level 1: <span class="text-danger">@{{location.google_admin_area_level_1_long}}</span></li>
                                  <li>level 2: <span class="text-danger">@{{location.google_admin_area_level_2_long}}</span></li>
                                  <li>level 3: <span class="text-danger">@{{location.google_admin_area_level_3_long}}</span></li>
                                </ul>
                              </li>
                              <li>locality: <span class="text-danger">@{{location.google_locality_long}}</span> </li>
                              <li>
                                sublocality
                                <ul>
                                  <li>level 1: <span class="text-danger">@{{location.google_sublocality_level_1_long}}</span></li>
                                  <li>level 2: <span class="text-danger">@{{location.google_sublocality_level_2_long}}</span></li>
                                </ul>
                              </li>
                              <li>postal code: <span class="text-danger">@{{location.google_postal_code}}</span></li>
                              <li>formatted name: <span class="text-danger">@{{location.formatted_address}}</span></li>
                              <li>location type: <span class="text-danger">@{{locationType(location)}}</span></li>
                            </ul>
                          </div>
                        </div>
                    </div>
                  </div>
                </td>
            </tr>
          </template>
        </tbody>
        </table>
       </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script src="{{mix('js/explained_location.js')}}"></script>
@endpush
