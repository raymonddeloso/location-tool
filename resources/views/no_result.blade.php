@extends('layout.master')
@section('content')
	<br>
    <br>
	<div class="row">
		<div class="container">
			<h3>No Result Locations {{\App\RawLocation::where('no_result',1)->count()}}
		</div>
	</div>
	<div class="container">

		<div class="row">
			<div class="col">
				<table class="table">
				<thead>
					<tr>
						<th><a href="#" id="location_name">Donut Loc.</a></th>
						<th>Coordinates</th>
						<th>State</th>
						<th>Donut Loc. Full</th>
						<th><a href="#" id="job_count">Job Count</a></th>
					</tr>
				</thead>
				<tbody>
					@foreach($locations as $location)

						<tr class="remove-{{$location->id}}">
							<td><a href="#{{$location->id}}" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">{{$location->indeed_location_name}}</a></td>
							<td>{{$location->indeed_coordinates}}</td>
							<td>{{$location->region}}</td>
							<td>{{$location->formatted_location_full}}</td>
							<td>{{$location->job_count}}</td>
						</tr>
						<tr class="collapse remove-{{$location->id}}" id="{{$location->id}}">
							<td colspan="5">
								<div class="row">
								<div class="col-sm-3">
				                    <div class="card">
				                      <div class="card-header">
				                        <h5 class="card-title text-primary">Donut Location Details</h5>
				                      </div>
				                      <div class="card-body">

				                        <ul class="list-unstyled">
				                          <li>name: <span class="text-primary">{{$location->indeed_location_name}}</span></li>
				                          <li>coordinates: <span class="text-primary">{{$location->indeed_coordinates}}</span></li>
				                          <li>state: <span class="text-primary">{{$location->state}}</span></li>
				                          <li>region: <span class="text-primary">{{$location->region}}</span></li>
				                          <li>formatted name: <span class="text-primary">{{$location->formatted_location_full}}</span></li>
				                          <li>postal code: <span class="text-primary">{{(substr_count($location->formatted_location_full, " ") ) ? explode(" ",$location->formatted_location_full)[0] : ""}}</span></li>
				                          <li>job count: <span class="text-primary">{{$location->job_count}}</span></li>
				                        </ul>
				                      </div>
				                      <div class="card-footer">
				                      	<div class="row">
				                      		<div class="col">
					                        	<!-- <button class="btn btn-sm btn-warning btn-block undecided-btn" data-id="{{$location->id}}">Undecided</button> -->
					                        </div>
				                      	</div>
				                      </div>
				                    </div>
				                  </div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{$locations->links("pagination::bootstrap-4")}}
			</div>
		</div>
	</div>
@endsection
@push('js')
	<script>
	$(document).ready(function(){
		$(document).on('click','#job_count',function(e){
			e.preventDefault();
			var a = $('<a>',{
	          href:window.location.href
	        });
	        if(a.prop('search').length == 0){
	        	window.location = '/no-result?sort=job_count&order=asc'
	        }else{
	        	window.location = '/no-result'
	        }

		});

		$(document).on('click','#location_name', function(e){
			e.preventDefault();

			var a = $('<a>',{
	          href:window.location.href
	        });

	        if(a.prop('search').length == 0) {
	        	window.location = '/no-result?sort=location_name&order=asc'
	        }else if(a.prop('search').indexOf("asc") > 0){
	        	window.location = '/no-result?sort=location_name&order=desc'
	        }else{

	        	window.location = '/no-result?sort=location_name&order=asc'
	        }
		});
	});
	</script>
@endpush
