@extends('layout.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-12">
    <br>
    <br>
    <h3>Undecided Locations {{\App\RawLocation::where('undecided',1)->count()}}</h3>
    <br>
      <table class="table">
        <thead class="thead-light">
          <tr>
            <th>Donut Loc</th>
            <th>Coordinates</th>
            <th>State</th>
            <th>Donut Loc. Full</th>
            <th>Job Count</th>
          </tr>
        </thead>
        <tbody>
			@foreach($locations as $location)
						<tr class="remove-{{$location->id}}">
							<td><a href="#{{$location->id}}" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">{{$location->indeed_location_name}}</a></td>
							<td>{{$location->indeed_coordinates}}</td>
							<td>{{$location->region}}</td>
							<td>{{$location->formatted_location_full}}</td>
							<td>{{$location->job_count}}</td>
						</tr>
						<tr class="collapse remove-{{$location->id}}" id="{{$location->id}}">
							<td colspan="5">
								<div class="row">
								<div class="col-sm-3">
				                    <div class="card">
				                      <div class="card-header">
				                        <h5 class="card-title text-primary">Donut Location Details</h5>
				                      </div>
				                      <div class="card-body">

				                        <ul class="list-unstyled">
				                          <li>name: <span class="text-primary">{{$location->indeed_location_name}}</span></li>
				                          <li>coordinates: <span class="text-primary">{{$location->indeed_coordinates}}</span></li>
				                          <li>state: <span class="text-primary">{{$location->state}}</span></li>
				                          <li>region: <span class="text-primary">{{$location->region}}</span></li>
				                          <li>formatted name: <span class="text-primary">{{$location->formatted_location_full}}</span></li>
				                          <li>postal code: <span class="text-primary">{{(substr_count($location->formatted_location_full, " ") ) ? explode(" ",$location->formatted_location_full)[0] : ""}}</span></li>
				                          <li>job count: <span class="text-primary">{{$location->job_count}}</span></li>
				                          <hr>
				                          <li>Sample Job Url's:
											<ul class="list-unstyled">
												@foreach($location->hasManyUrl as $url)
													<li style="font-size: 14px" class="text-primary">{{$url->url}}</li>
												@endforeach
											</ul>
				                          </li>
				                        </ul>
				                      </div>
				                    </div>
				                  </div>
								@foreach(getUnexplainedLocationResult($location->indeed_location_name) as $gloc)

									<div class="col-sm-3">
					                    <div class="card" >
					                      <div class="card-header">
					                        <h5 class="card-title text-danger">Google Location Details</h5>
					                      </div>
					                      <div class="card-body">
					                        <ul class="list-unstyled">
					                          <li>name: <span class="text-danger">{{$gloc->google_location_name}}</span></li>
					                          <li>coordinates: <span class="text-danger">{{(float)$gloc->google_location_lat}}, {{(float)$gloc->google_location_lng}}</span></li>
					                          <li>
					                            administrative area:
					                            <ul>
					                              <li>level 1: <span class="text-danger">{{$gloc->google_admin_area_level_1_long}}</span></li>
					                              <li>level 2: <span class="text-danger">{{$gloc->google_admin_area_level_2_long}}</span></li>
					                              <li>level 3: <span class="text-danger">{{$gloc->google_admin_area_level_3_long}}</span></li>
					                            </ul>
					                          </li>
					                          <li>locality: <span class="text-danger">{{$gloc->google_locality_long}}</span> </li>
					                          <li>
					                            sublocality
					                            <ul>
					                              <li>level 1: <span class="text-danger">{{$gloc->google_sublocality_level_1_long}}</span></li>
					                              <li>level 2: <span class="text-danger">{{$gloc->google_sublocality_level_2_long}}</span></li>
					                            </ul>
					                          </li>
					                          <li>postal code: <span class="text-danger">{{$gloc->google_postal_code}}</span></li>
					                          <li>formatted name: <span class="text-danger">{{$gloc->formatted_address}}</span></li>
					                          <li>location type: <span class="text-danger">{{locationType($gloc)}}</span></li>
					                          <hr>
					                          <li>haversine: <span class="text-danger">{{calculateHaversine($location->indeed_coordinates,(float)$gloc->google_location_lat,(float)$gloc->google_location_lng)}}</span></li>
					                        </ul>
					                      </div>
					                      <div class="card-footer">
					                        <div class="row">
					                        	<div class="col">
						                        	<button class="btn btn-sm btn-primary btn-block select-btn" data-raw-id="{{$location->id}}" data-gg="{{$gloc->id}}">Select</button>
						                        </div>
					                        </div>
					                      </div>
					                    </div>
				                  	</div>
								@endforeach
								</div>
							</td>
						</tr>
					@endforeach
        </tbody>
      </table>
       {{$locations->links("pagination::bootstrap-4")}}
    </div>
  </div>
</div>
@endsection

@push('js')
	<script>
		$(document).ready(function(){
			$('.select-btn').on('click', function(e){
				e.preventDefault();

				var locId = $(this).attr('data-raw-id');
				var ggId = $(this).attr('data-gg');

				swal({
		          title: "Are you sure?",
		          text: "You are about to assign this location!",
		          icon: "warning",
		          buttons: true,
		          dangerMode: false,
		        })
		        .then((willDelete) => {
		          if (willDelete) {
		          	$.ajax({
		                type:'POST',
		                url:'{{route("decide-loc")}}',
		                headers:{
		                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		                },
		                data:{donut_id:locId,gg_id:ggId},
		                success:function(data){
		                  console.log(data);
		                  $('.remove-'+locId).remove();
		                  swal(data.indeed_location_name + " Successfully Assigned! ", {
		                    icon: "success",
		                  });
		                }

		            });
		          } else {

		          }
		        });
			});
		});
	</script>
@endpush
