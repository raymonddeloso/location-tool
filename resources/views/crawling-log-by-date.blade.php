@extends('layout.master')
@section('content')
	@section('content')
	<br>
    <br>
	<div class="row">
		<div class="container">
			<h3>Crawled Locations</h3>
		</div>
	</div>
	<div class="container">

			<ul class="nav nav-tabs" id="myTab" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" id="explained-tab" data-toggle="tab" href="#explained" role="tab" aria-controls="explained" aria-selected="true">
			    	Explained
			    </a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="unexplained-tab" data-toggle="tab" href="#unexplained" role="tab" aria-controls="unexplained" aria-selected="false">Unexplained</a>
			  </li>
			 <!--  <li class="nav-item">
			   <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
			 </li> -->
			</ul>
			<div class="tab-content" id="myTabContent">
			  <div class="tab-pane fade show active" id="explained" role="tabpanel" aria-labelledby="explained-tab">
			  	<div class="container">

			  		<div class="row">
			  			<div id="explained-content-view">
							<table class="table">
								<thead class="thead-light">
									<tr>
										<th>Donut Location</th>
							            <th>Coordinates</th>
							            <th>GG location</th>
							            <th>Haversine</th>
							            <th>Location Type</th>
							            <th>Match Type</th>
							            <th>Options</th>
									</tr>
								</thead>
								<tbody>
									@foreach($byDateLogsExplained as $location)
										<tr>
											<td>{{$location->indeed_location_name}}</td>
											<td>{{$location->indeed_coordinates}}</td>
											<td>{{(is_numeric($location->google_location_name) ) ? $location->formatted_address : $location->google_location_name}}</td>
											<td>{{$location->gg_equivalent_haversine_distance}}</td>
											<td>{{locationType($location)}}</td>
											<td>{{$location->explanation_type}}</td>
											<td>
												<a data-toggle="collapse" href="#{{$location->raw_id}}" role="button" aria-expanded="false" aria-controls="collapseExample">Details</a> |
									            <a href="#" class="remove-btn" data-id="{{$location->raw_id}}"><i class="fas fa-trash-alt"></i></a> |
									            <a href="#" class="undecided-btn"><i class="fas fa-question-circle pop" data-id="{{$location->raw_id}}" data-container="body" data-toggle="popover" data-placement="top" data-content="Move the specific location to undecided?"></i></a>
									        </td>
										</tr>
										<tr class="collapse remove-{{$location->raw_id}}" id="{{$location->raw_id}}">
								            <td colspan="7">
								              <div class="card card-body">
								                <div class="row">

								                {{-- Start Indeed Details --}}
								                  <div class="col">
								                    <div class="card">
								                      <div class="card-header">
								                        <h5 class="card-title text-primary">Indeed Location Details</h5>
								                      </div>
								                      <div class="card-body">

								                        <ul class="list-unstyled">
								                          <li>name: <span class="text-primary">{{$location->indeed_location_name}}</span></li>
								                          <li>coordinates: <span class="text-primary">{{$location->indeed_coordinates}}</span></li>
								                          <li>state: <span class="text-primary">{{$location->state}}</span></li>
								                          <li>region: <span class="text-primary">{{$location->region}}</span></li>
								                          <li>formatted name: <span class="text-primary">{{$location->formatted_location_full}}</span></li>
								                          <li>postal code: <span class="text-primary">{{(substr_count($location->formatted_location_full, " ") ) ? explode(" ",$location->formatted_location_full)[0] : ""}}</span></li>
								                          <li>job count: <span class="text-primary">{{$location->job_count}}</span></li>
								                        </ul>
								                      </div>
								                    </div>
								                  </div>

								                {{-- Start Google Details --}}
								                  <div class="col">
								                    <div class="card">
								                      <div class="card-header">
								                        <h5 class="card-title text-danger">Google Location Details</h5>
								                      </div>
								                      <div class="card-body">
								                        <ul class="list-unstyled">
								                          <li>name: <span class="text-danger">{{$location->google_location_name}}</span></li>
								                          <li>coordinates: <span class="text-danger">{{(float)$location->google_location_lat}}, {{(float)$location->google_location_lng}}</span></li>
								                          <li>
								                            administrative area:
								                            <ul>
								                              <li>level 1: <span class="text-danger">{{$location->google_admin_area_level_1_long}}</span></li>
								                              <li>level 2: <span class="text-danger">{{$location->google_admin_area_level_2_long}}</span></li>
								                              <li>level 3: <span class="text-danger">{{$location->google_admin_area_level_3_long}}</span></li>
								                            </ul>
								                          </li>
								                          <li>locality: <span class="text-danger">{{$location->google_locality_long}}</span> </li>
								                          <li>
								                            sublocality
								                            <ul>
								                              <li>level 1: <span class="text-danger">{{$location->google_sublocality_level_1_long}}</span></li>
								                              <li>level 2: <span class="text-danger">{{$location->google_sublocality_level_2_long}}</span></li>
								                            </ul>
								                          </li>
								                          <li>postal code: <span class="text-danger">{{$location->google_postal_code}}</span></li>
								                          <li>formatted name: <span class="text-danger">{{$location->formatted_address}}</span></li>
								                          <li>location type: <span class="text-danger">{{locationType($location)}}</span></li>
								                        </ul>
								                      </div>
								                    </div>
								                  </div>
								                </div>
								              </div>
								            </td>
								          </tr>
									@endforeach
								</tbody>
							</table>
							{{$byDateLogsExplained->links("pagination::bootstrap-4")}}
						</div>
				  	</div>

			  	</div>
			  </div>
			  <div class="tab-pane fade" id="unexplained" role="tabpanel" aria-labelledby="unexplained-tab">
			  	<div class="container">
			  		<div class="row">
			  			<div id="unexplained-content-view">
				  			<table class="table">
								<thead>
									<tr>
										<th><a href="#" id="location_name">Donut Loc.</a></th>
										<th>Coordinates</th>
										<th>State</th>
										<th>Donut Loc. Full</th>
										<th><a href="#" id="job_count">Job Count</a></th>
									</tr>
								</thead>
								<tbody>
									@foreach($byDateLogsUnexplained as $location)
										<tr class="remove-{{$location->id}}">
											<td><a href="#{{$location->id}}" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">{{$location->indeed_location_name}}</a></td>
											<td>{{$location->indeed_coordinates}}</td>
											<td>{{$location->region}}</td>
											<td>{{$location->formatted_location_full}}</td>
											<td>{{$location->job_count}}</td>
										</tr>
										<tr class="collapse remove-{{$location->id}}" id="{{$location->id}}">
											<td colspan="5">
												<div class="row">
												<div class="col-sm-3">
								                    <div class="card">
								                      <div class="card-header">
								                        <h5 class="card-title text-primary">Indeed Location Details</h5>
								                      </div>
								                      <div class="card-body">

								                        <ul class="list-unstyled">
								                          <li>name: <span class="text-primary">{{$location->indeed_location_name}}</span></li>
								                          <li>coordinates: <span class="text-primary">{{$location->indeed_coordinates}}</span></li>
								                          <li>state: <span class="text-primary">{{$location->state}}</span></li>
								                          <li>region: <span class="text-primary">{{$location->region}}</span></li>
								                          <li>formatted name: <span class="text-primary">{{$location->formatted_location_full}}</span></li>
								                          <li>postal code: <span class="text-primary">{{(substr_count($location->formatted_location_full, " ") ) ? explode(" ",$location->formatted_location_full)[0] : ""}}</span></li>
								                          <li>job count: <span class="text-primary">{{$location->job_count}}</span></li>
								                          <hr>
								                          <li>Sample Job Url's:
															<ul class="list-unstyled">
																@foreach($location->hasManyUrl as $url)
																	<li style="font-size: 14px" class="text-primary">{{$url->url}}</li>
																@endforeach
															</ul>
								                          </li>
								                        </ul>
								                      </div>
								                      <div class="card-footer">
								                      	<div class="row">
								                      		<div class="col">
									                        	<button class="btn btn-sm btn-warning btn-block undecided-btn" data-id="{{$location->id}}">Undecided</button>
									                        </div>
								                      	</div>
								                      </div>
								                    </div>
								                  </div>
												@foreach(getUnexplainedLocationResult($location->indeed_location_name) as $gloc)
													<div class="col-sm-3">
									                    <div class="card" >
									                      <div class="card-header">
									                        <h5 class="card-title text-danger">Google Location Details</h5>
									                      </div>
									                      <div class="card-body">
									                        <ul class="list-unstyled">
									                          <li>name: <span class="text-danger">{{$gloc->google_location_name}}</span></li>
									                          <li>coordinates: <span class="text-danger">{{(float)$gloc->google_location_lat}}, {{(float)$gloc->google_location_lng}}</span></li>
									                          <li>
									                            administrative area:
									                            <ul>
									                              <li>level 1: <span class="text-danger">{{$gloc->google_admin_area_level_1_long}}</span></li>
									                              <li>level 2: <span class="text-danger">{{$gloc->google_admin_area_level_2_long}}</span></li>
									                              <li>level 3: <span class="text-danger">{{$gloc->google_admin_area_level_3_long}}</span></li>
									                            </ul>
									                          </li>
									                          <li>locality: <span class="text-danger">{{$gloc->google_locality_long}}</span> </li>
									                          <li>
									                            sublocality
									                            <ul>
									                              <li>level 1: <span class="text-danger">{{$gloc->google_sublocality_level_1_long}}</span></li>
									                              <li>level 2: <span class="text-danger">{{$gloc->google_sublocality_level_2_long}}</span></li>
									                            </ul>
									                          </li>
									                          <li>postal code: <span class="text-danger">{{$gloc->google_postal_code}}</span></li>
									                          <li>formatted name: <span class="text-danger">{{$gloc->formatted_address}}</span></li>
									                          <li>location type: <span class="text-danger">{{locationType($gloc)}}</span></li>
									                          <hr>
									                          <li>haversine: <span class="text-danger">{{calculateHaversine($location->indeed_coordinates,(float)$gloc->google_location_lat,(float)$gloc->google_location_lng)}}</span></li>
									                        </ul>
									                      </div>
									                      <div class="card-footer">
									                        <div class="row">
									                        	<div class="col">
										                        	<button class="btn btn-sm btn-primary btn-block select-btn" data-raw-id="{{$location->id}}" data-gg="{{$gloc->id}}">Select</button>
										                        </div>
									                        </div>
									                      </div>
									                    </div>
								                  	</div>
												@endforeach
												</div>
											</td>
										</tr>
									@endforeach
								</tbody>
				  			</table>
				  			{{$byDateLogsUnexplained->links("pagination::bootstrap-4")}}
			  			</div>
			  		</div>


			  	</div>
			  </div>
			 <!--  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab"></div> -->
			</div>
	</div>
@endsection
@push('js')
<script>
  $(document).ready(function(){

    $(document).on('click', '.remove-btn', function(e){
        e.preventDefault();
        var locId = $(this).attr('data-id');

        swal({
          title: "Ooops!",
          text: "You are about to remove this location!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                type:'POST',
                url:'{{route("remove-explained")}}',
                headers:{
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{id:locId},
                success:function(data){

                  $('.remove-'+locId).remove();
                  swal(data.indeed_location_name + " Successfully Removed! ", {
                    icon: "success",
                  }).then((value) => {
                    paginate(getUrl());
                    getCount();
                  });
                }

            });
          } else {

          }
        });

    });

    $(document).on("click", ".pop", function(e){
      e.preventDefault();
      var locId = $(this).attr('data-id');

      swal({
        title: "Hey!",
        text: "You are about to move this location to undecided!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            $.ajax({
              type:'POST',
              url:'{{route("undecide-loc")}}',
              headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data:{id:locId},
              success:function(data){
                console.log(data);
                swal(data.indeed_location_name + " Successfully move to Undecided!",{
                  icon:"success"
                }).then((value) => {
                  paginate(getUrl());
                  getCount();
                });
              }
            });
        } else {

        }
      });
  	});

    $(document).on('click', '.select-btn', function(){

		var locId = $(this).attr('data-raw-id');
		var ggId = $(this).attr('data-gg');

		swal({
          title: "Are you sure?",
          text: "You are about to assign this location!",
          icon: "warning",
          buttons: true,
          dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {
          	$.ajax({
                type:'POST',
                url:'{{route("explain-loc")}}',
                headers:{
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{donut_id:locId,gg_id:ggId},
                success:function(data){
                  $('.remove-'+locId).remove();
                  swal(data.indeed_location_name + " Successfully Assigned! ", {
                    icon: "success",
                  }).then((value) => {
                  	location.reload();
                  });
                }

            });
          } else {

          }
        });
	});

	$(document).on('click','.undecided-btn',function(){
		var locId = $(this).attr('data-id');

		swal({
          title: "Move to undecided?",
          text: "This location will be moved to undecided! continue?",
          icon: "warning",
          buttons: true,
          dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {
          	$.ajax({
          		type:'POST',
          		url:'{{route("undecide-loc")}}',
          		headers:{
          			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          		},
          		data:{id:locId},
          		success:function(data){
          			console.log(data);
          			$('.remove-'+locId).remove();
          			swal(data.indeed_location_name + " Successfully move to Undecided! ", {
	                	icon: "success",
	                }).then((value) => {
	                	location.reload();
	                });
          		}
          	});
          } else {

          }
        });
	});

    $(document).on('click','li.page-item', function(e){
        var url = $('a',this).attr('href');
        e.preventDefault();
        if(!$(this).hasClass("active")){

          	var currentTab = $('a.nav-link.active').attr('href');

          	if(currentTab == "#explained"){
          		$.ajax({
	                type:'GET',
	                url:url,
	                dataType:"html",
	                success:function(data){
	                  $('#explained-content-view').html(data);
	                }
	              });
          		window.history.pushState("", "", url);
          	}

          	if(currentTab == "#unexplained"){
          		$.ajax({
	                type:'GET',
	                url:url,
	                dataType:"html",
	                success:function(data){

	                  $('#unexplained-content-view').html(data);
	                }
	              });
          		window.history.pushState("", "", url);
          	}
          //paginate(url);
          //window.history.pushState("", "", url);
        }

    });

          /*paginate = function(url) {
            $.ajax({
                type:'GET',
                url:url,
                dataType:"html",
                success:function(data){
                  $('#content-view').html(data);
                }
              });
          }

          getUrl = function(){
            var a = $('<a>',{
              href:window.location.href
            });
            return a.prop('pathname')+a.prop('search');
          }

          getCount = function(){
            $.ajax({
              type:"GET",
              url:"/api/explianed-count",
              success:function(data){
                $('#explained-count').html(data);
              }
            })
          }*/
  });
</script>
@endpush
