@extends('layout.master')
@section('content')
	@section('content')
	<br>
    <br>
	<div class="row">
		<div class="container">
			<h3>
		</div>
	</div>
	<div class="container">

		<div class="row">
			<div class="col">
				<table class="table">
				<thead>
					<tr>
						<th>Total Locations</th>
						<th>New Locations</th>
						<th>Explained By Zip</th>
						<th>Explained By Name</th>
						<th>Unexplained</th>
						<th>No Result</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@foreach($locationlogs as $location)

						<tr class="remove-{{$location->id}}">
							<td>{{$location->total_locations_count}}</td>
							<td>{{$location->new_locations_found}}</td>
							<td>
							{{$rawLocationModel::where('is_explained',1)
							->where('explanation_type','zip')
							->where('batch_num',explode(" ",$location->created_at)[0])->count()}}
							</td>
							<td>
								{{$rawLocationModel::where('is_explained',1)
							->where('explanation_type','name')
							->where('batch_num',explode(" ",$location->created_at)[0])->count()}}
							</td>
							<td>
								{{$rawLocationModel::where('is_explained',0)
							->where('batch_num',explode(" ",$location->created_at)[0])->count()}}
							</td>
							<td>{{$location->zero_result}}</td>
							<td><a href="{{route('crawling-logs-by-date',explode(" ",$location->created_at)[0])}}">{{explode(" ",$location->created_at)[0]}}</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{$locationlogs->links("pagination::bootstrap-4")}}
			</div>
		</div>
	</div>
@endsection

