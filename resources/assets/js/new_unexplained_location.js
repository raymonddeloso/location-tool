const urls = {
	index:'/api/unexplained-location/index',
	googleAddress:'/api/unexplained-location/google-addresses',
	undecided:'/api/undecide-locations',
	explain:'/api/explian-locations',
	getJobKeys:'/api/unexplained-location/get-jobkey'
};

const data = {
	locations:[],
	selectedLocation:[],
	current_page_url:"",
	next_page_url:"",
	prev_page_url:"",
	googleAddresses:[],
	selectedId:"",
	total:0,
	jobKeys:[],
	jobTitle:"",
	jobDescription:"",
	jobKey:"",
	company:"",
	viewJobKeyText:"View JobKeys",
	visibleGoogleAddresses:false
};

const methods = {
	index() {
		axios.get(urls.index)
		.then((response) => {
			this.locations = response.data.data;
			this.next_page_url = response.data.next_page_url;
			this.prev_page_url = response.data.prev_page_url;
			this.current_page_url = response.data.path+"?page="+response.data.current_page;
			this.total = response.data.total;
		})
		.catch((error) => {
			console.log(error)
		})
	},

	reset() {
		this.locations = [];
		this.selectedLocation = [];
		this.googleAddresses = [];
		this.total = 0;
		this.jobTitle = "";
		this.jobDescription = "";
		this.viewJobKeyText = "View JobKeys";
		this.visibleGoogleAddresses = false;
	},

	paginate(page) {
		this.reset();
		axios.get(data[page])
		.then((response) => {
			this.locations = response.data.data;
			this.next_page_url = response.data.next_page_url;
			this.prev_page_url = response.data.prev_page_url;
			this.current_page_url = response.data.path+"?page="+response.data.current_page;
			this.total = response.data.total;
		})
		.catch((error) => {
			console.log(error)
		})
	},
	getDetails(key){
		if(this.selectedLocation.id == this.locations[key].id){
			this.selectedLocation = [];
			this.googleAddresses = [];
			this.selecteId = "";
		}else {
			this.selectedLocation = this.locations[key];
			this.selecteId = this.selectedLocation.id;
			this.jobKeys = [];
			
			this.getGoogleAddresses(this.selectedLocation.indeed_location_name);
		}
		
	},
	getGoogleAddresses(location) {
		axios.get(urls.googleAddress,{
			params:{
				location:location
			}
		})
		.then((response) => {
			console.log(response);
			this.googleAddresses = response.data;
			this.visibleGoogleAddresses = true;
		})
		.catch((error) => {
			console.log(error);
			alert('Unable to Fetch Google Addresses!');
		})
	},
	locationType(location) {
		if(location.is_sublocality){
			if(location.google_sublocality_level_2_long === null) {
				return "Sublocality Level 1";
			}
			return "Sublocality Level 2";
		}

		if (location.is_locality) {
	        return "Locality";
	    }

	    if(location.is_admin_area) {
	    	if (location.google_admin_area_level_3_long === null && location.google_admin_area_level_2_long === null) {
	            return "Admin Area Level 1";
	        }
	        if (location.google_admin_area_level_3_long === null && location.google_admin_area_level_2_long !== null) {
	            return "Admin Area Level 2";
	        }
	        if (location.google_admin_area_level_3_long !== null) {
	            return "Admin Area Level 3";
	        }
	    }
	},

	haversince(lat1,lon1,coords) {
		const R = 6371;
		coords = coords.split(",");

		const lat2 = parseFloat(coords[0]);
		const lon2 = parseFloat(coords[1]);

		dLat = (lat2- lat1) * Math.PI / 180;
		dLon = (lon2 - lon1) * Math.PI / 180;

		const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				  Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
				  Math.sin(dLon / 2) * Math.sin(dLon / 2);
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return (R * c).toFixed(3);
	},

	undecided() {
		swal({
          title: "Move to undecided?",
          text: "This location will be moved to undecided! continue?",
          icon: "warning",
          buttons: true,
          dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {
          		axios.post(urls.undecided,{
          			id:this.selectedLocation.id
          		})
          		.then((response) => {
          			this.reset();
	          		this.paginate('current_page_url')
	      			swal(" Successfully move to Undecided! ", {
	                	icon: "success",
	                });
          		})
          		.catch((error) => {
          			alert('Failed to move to Undecided!');
          		})
          		
          } 
        });
	},

	save(googleId) {

		swal({
          title: "Are you sure?",
          text: "You are about to assign this location!",
          icon: "warning",
          buttons: true,
          dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {
          		axios.post(urls.explain,{
          			donut_id:this.selectedLocation.id,
          			gg_id:googleId
          		})
          		.then((response) => {
          			this.reset()
          			this.paginate('current_page_url');
          			swal(" Successfully move to Assigned! ", {
	                	icon: "success",
	                });
          		})
          		.catch((error) => {
          			alert('Failed to assign Location!');
          		})
          }
        });
	},

	getJobKeys(locationName,coordinates) {
		this.viewJobKeyText="Fetching JobKeys...";
		axios.get(urls.getJobKeys,{
			params:{
				city:locationName,
				coordinates:coordinates
			}
		})
		.then((response) => {
			this.viewJobKeyText = "View JobKeys";
			this.jobKeys = response.data;
		})
	},

	clickJobKey(key) {
		this.company = this.jobKeys[key].company;
		this.jobKey = this.jobKeys[key].jobkey;
		this.jobTitle = this.jobKeys[key].title;
		this.jobDescription = this.jobKeys[key].description;
	},

	closeJobContainer() {
		this.jobTitle = "";
		this.jobDescription = "";
	},
	closeGoogleAddresses() {
		this.visibleGoogleAddresses = false;
	}

};	


	

const vm = new Vue({
	el:'#app',
	data:data,
	methods:methods,
	mounted(){
		this.index();
	}
});