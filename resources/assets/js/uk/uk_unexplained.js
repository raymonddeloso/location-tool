import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

const urls = {
	index:'/api/uk/unexplained-index',
	search:'/api/uk/search-unexplained',
	getjobkeys:'/api/uk/unexplained-jobkeys',
	googlelocations:'/api/uk/google-locations',
	assignloc:'/api/uk/assign-loc',
	undecideloc:'/api/uk/undecide-loc'
};

const data = {
	unexplainedLocations:{},
	next_page_url:"",
	selectedKey:"",
	searchTerm:"",
	count:"",
	selectedLocation:"",
	viewJobKeyText:"View JobKeys",
	jobKeys:[],
	jobTitle:"",
	jobDescription:"",
	jobKey:"",
	company:"",
	city:"",
	googleAddresses:[],
	visibleGoogleAddresses:false
};

const methods = {
	index() {
		axios.get(urls.index)
		.then((response) => {
			this.unexplainedLocations = response.data.data;
			this.next_page_url = response.data.next_page_url;
			this.count = response.data.total;
		})
		.catch((error) => {
			console.log(error)
		})
	},

	scroll(unexplainedLocations){
		window.onscroll = () => {
			let bottomOfwindow = document.documentElement.scrollTop + window.innerHeight === document.documentElement.offsetHeight;

			if(bottomOfwindow) {				
				axios.get(this.next_page_url)
				.then((response) => {
					for (let i = response.data.data.length - 1; i >= 0; i--) {
						this.unexplainedLocations.push(response.data.data[i]);
					}					
					this.next_page_url = response.data.next_page_url;
				})
			}
		}
	},

	locationType(location){
		if(location.is_sublocality){
			if(location.google_sublocality_level_2_long == null) {
				return "Sublocality Level 1";
			}
			return "Sublocality Level 2";
		}

		if(location.is_locality){
			return "Locality";
		}

		if(location.is_admin_area){
			if(location.google_admin_area_level_3_long == null &&
				location.google_admin_area_level_2_long == null) {
				return "Admin Area Level 1";
			}

			if(location.google_admin_area_level_3_long == null &&
				location.google_sublocality_level_2_long != null) {
				return "Admin Area Level 2";
			}

			if(location.google_admin_area_level_3_long != null) {
				return "Admin Area Level 3";
			}
		}
		
	},

	search(){
		if(this.searchTerm.length >= 2 ) {
			axios.get(urls.search,{
				params:{
					term:this.searchTerm
				}
			})
			.then((response) => {
				this.unexplainedLocations = response.data.data;
				this.count = response.data.total;
				this.next_page_url = response.data.next_page_url;
			})
		}

		if(this.searchTerm.length == 0) {
			this.index();
		}
	},
	selectLocation(key){
		
	},

	getJobKeys(locationName,coordinates) {
		this.viewJobKeyText="Fetching JobKeys...";
		axios.get(urls.getjobkeys,{
			params:{
				city:locationName,
				coordinates:coordinates
			}
		})
		.then((response) => {
			this.viewJobKeyText = "View JobKeys";
			this.jobKeys = response.data;
		})
		.catch((error) => {
			console.log(error);
		})
	},

	clickJobKey(key) {
		this.company = this.jobKeys[key].company;
		this.jobKey = this.jobKeys[key].jobkey;
		this.jobTitle = this.jobKeys[key].title;
		this.jobDescription = this.jobKeys[key].description;
		this.city = this.jobKeys[key].city;
	},

	closeJobContainer() {
		this.company = "";
		this.jobKey = "";
		this.jobTitle = "";
		this.jobDescription = "";
		this.city = "";
	},

	closeGoogleAddresses() {
		this.visibleGoogleAddresses = false;
		this.googleAddresses = [];
		this.selectedLocation = "";
	},

	getDetails(key) {		

		if(this.selectedLocation.id == this.unexplainedLocations[key].id) {
			this.selectedLocation = "";
			this.googleAddresses = [];
			this.visibleGoogleAddresses = false;
		} else {
			this.selectedLocation = this.unexplainedLocations[key];
			this.getGoogleAddresses(this.selectedLocation.indeed_location_name);
		}
	},
	getGoogleAddresses(locations){
		axios.get(urls.googlelocations,{
			params:{
				location:locations
			}
		})
		.then((response) => {
			this.googleAddresses = response.data;
			this.visibleGoogleAddresses = true;
		})
	},
	haversince(lat1,lon1,coords) {
		const R = 6371;
		coords = coords.split(",");

		const lat2 = parseFloat(coords[0]);
		const lon2 = parseFloat(coords[1])

		let dLat = (lat2- lat1) * Math.PI / 180;
		let dLon = (lon2 - lon1) * Math.PI / 180;

		const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				  Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
				  Math.sin(dLon / 2) * Math.sin(dLon / 2);
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return (R * c).toFixed(3);
	},
	save(googleId) {
		Swal.fire({
		  title: "Are you sure?",
		  text: "You are about to assign this location!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Assign it!'
		}).then((result) => {
		  if (result.value) {	   

		    axios.post(urls.assignloc,{
		    	id:this.selectedLocation.id,
		    	gg_id:googleId
		    })
		    .then((response) => {
		    	this.googleAddresses = [];
		    	this.visibleGoogleAddresses = false;
		    	this.selectedLocation = "";
		    	this.index();
		    	Swal.fire(
			      'Assigned!',
			      'Location successfully assigned!',
			      'success'
			    );
		    })
		    .catch((error) => {

		    })

		  }
		});	
	},
	undecided(){
		Swal.fire({
		  title: "Are you sure?",
		  text: "You are about to move this location UNDECIDED!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Move it!'
		}).then((result) => {
		  if (result.value) { 
		  	axios.post(urls.undecideloc,{
		  		id:this.selectedLocation.id
		  	})
		  	.then((response) => {
		  		this.googleAddresses = [];
		    	this.visibleGoogleAddresses = false;
		    	this.selectedLocation = "";
		    	this.index();
		    	Swal.fire(
			      'Moved!',
			      'Location successfully moved to UNDECIDED!',
			      'success'
			    );
		  	})
		  	.catch((error) => {
		  		console.log(error);
		  	})
		  }
		});
	}

};

const vm = new Vue({
	el:'#app',
	data:data,
	methods:methods,
	mounted(){
		this.index();
		this.scroll(this.unexplainedLocations);
	}
});