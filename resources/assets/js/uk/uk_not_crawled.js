const urls = {
	notcrawled:'/api/uk/not-crawled'
};

const data = {
	uncrawledLocations:[],
	count:0,
	next_page_url:""
};

const methods = {
	index(){
		axios.get(urls.notcrawled)
		.then((response) => {	
			this.uncrawledLocations = response.data.data;
			this.count = response.data.total;
			this.next_page_url = response.data.next_page_url;
		})	
		.catch((error) => {
			console.log(error)
		})
	}
};

const vm = new Vue({
	el:'#app',
	data:data,
	methods:methods,
	mounted(){
		this.index();
	}
});