import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

const urls = {
	index:'/api/uk/undecided-index',
	googlelocations:'/api/uk/google-locations',
	assignloc:'/api/uk/assign-loc'

};

const data = {
	undecidedLocations:[],
	count:0,
	next_page_url:"",
	selectedLocation:"",
	googleLocations:[],
};

const methods = {
	index(){
		axios.get(urls.index)
		.then((response) => {
			this.undecidedLocations = response.data.data;
			this.count = response.data.total;
			this.next_page_url = response.data.next_page_url;
		})
		.catch((error) => {
			console.log(error);
		})
	},
	getDetails(location){
		if(this.selectedLocation.length == 0 || this.selectedLocation.id != location.id){
			this.selectedLocation = location;
			this.googleLocations = [];
			this.getSuggestedGoogleLocations(location.indeed_location_name);
		}else{
			this.selectedLocation = "";
		}				
	},
	getSuggestedGoogleLocations(location){
		axios.get(urls.googlelocations,{
			params:{
				location:location
			}
		})
		.then((response)=>{
			this.googleLocations = response.data;
		})
		.catch((error) => {
			console.log(error);
		})
	},
	haversince(lat1,lon1,coords) {
		const R = 6371;
		coords = coords.split(",");

		const lat2 = parseFloat(coords[0]);
		const lon2 = parseFloat(coords[1])

		let dLat = (lat2- lat1) * Math.PI / 180;
		let dLon = (lon2 - lon1) * Math.PI / 180;

		const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				  Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
				  Math.sin(dLon / 2) * Math.sin(dLon / 2);
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return (R * c).toFixed(3);
	},
	locationType(location){
		if(location.is_sublocality){
			if(location.google_sublocality_level_2_long == null) {
				return "Sublocality Level 1";
			}
			return "Sublocality Level 2";
		}

		if(location.is_locality){
			return "Locality";
		}

		if(location.is_admin_area){
			if(location.google_admin_area_level_3_long == null &&
				location.google_admin_area_level_2_long == null) {
				return "Admin Area Level 1";
			}

			if(location.google_admin_area_level_3_long == null &&
				location.google_sublocality_level_2_long != null) {
				return "Admin Area Level 2";
			}

			if(location.google_admin_area_level_3_long != null) {
				return "Admin Area Level 3";
			}
		}
		
	},
	assign(gloc){
		axios.post(urls.assignloc,{
			id:selectedLocation.id,
			ggg_id:gloc.id
		})
		.then((response) => {
			Swal.fire(
			      'Assigned!',
			      'Location successfully assigned!',
			      'success'
			    );
			this.index();
		})
	}
};

const vm = new Vue({
	el:'#app',
	data:data,
	methods:methods,
	mounted(){
		this.index();
	}
});