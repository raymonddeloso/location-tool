import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

const urls = {
	index:'/api/uk/explained-index',
	search:'/api/uk/search-explained',
	undecide:'/api/uk/undecide-loc',
	unexplain:'/api/uk/unexplain-loc'
};

const data = {
	explainedLocations:[],
	next_page_url:"",
	count:"",
	searchTerm:"",
	selectedKey:""
};

const methods  = {
	index(){
		axios.get(urls.index)
		.then((response) => {
			this.explainedLocations = response.data.data;
			this.next_page_url = response.data.next_page_url;
			this.count = response.data.total;
		})
		.catch((error) => {
			console.log(error)
		})
	},

	locationType(location){
		if(location.is_sublocality){
			if(location.google_sublocality_level_2_long == null) {
				return "Sublocality Level 1";
			}
			return "Sublocality Level 2";
		}

		if(location.is_locality){
			return "Locality";
		}

		if(location.is_admin_area){
			if(location.google_admin_area_level_3_long == null &&
				location.google_admin_area_level_2_long == null) {
				return "Admin Area Level 1";
			}

			if(location.google_admin_area_level_3_long == null &&
				location.google_sublocality_level_2_long != null) {
				return "Admin Area Level 2";
			}

			if(location.google_admin_area_level_3_long != null) {
				return "Admin Area Level 3";
			}
		}
		
	},

	scroll(explainedLocations){
		window.onscroll = () => {
			let bottomOfwindow = document.documentElement.scrollTop + window.innerHeight === document.documentElement.offsetHeight;

			if(bottomOfwindow) {
				axios.get(this.next_page_url)
				.then((response) => {
					for (let i = response.data.data.length - 1; i >= 0; i--) {
						this.explainedLocations.push(response.data.data[i]);
					}					
					this.next_page_url = response.data.next_page_url;
				})
			}
		}
	},

	viewDetails(key){
		if(this.selectedKey.length == 0 || this.selectedKey != key){
			this.selectedKey = key;
		} else {
			this.selectedKey = "";
		}		
	},

	moveToUnexplained(location){
		Swal.fire({
		  title: 'Hey!',
		  text: "You are about to UNEXPLAIN this location!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, UNEXPLAIN it!'
		}).then((result) => {
		  if (result.value) {
			axios.post(urls.unexplain,{
				id:location.raw_id
			})
			.then((response) => {
				this.index();
				Swal.fire(
			      'UNEXPLAINED!',
			      'Location successfully moved to UNEXPLAINED!',
			      'success'
			    );
			})
			.catch((error) => {
				console.log(error);
			})
		  }
		});
	},

	moveToUndecided(location){
		Swal.fire({
		  title: 'Hey!',
		  text: "You are about to move this location to UNDECIDED!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Move it!'
		}).then((result) => {
		  if (result.value) {
			axios.post(urls.undecide,{
				id:location.raw_id
			})
			.then((response) => {
				this.index();
				Swal.fire(
			      'Moved!',
			      'Location successfully moved to UNDECIDED!',
			      'success'
			    );
			})
			.catch((error) => {
				console.log(error);
			})
		  }
		});
	},

	search(){
		if(this.searchTerm.length >= 2) {
			axios.get(urls.search,{
				params:{
					term:this.searchTerm
				}
			})
			.then((response) => {
				this.explainedLocations = response.data.data;
				this.next_page_url = response.data.next_page_url;
				this.count = response.data.total;
			})
			.catch((error) => {
				console.log(error);
			})
		}

		if(this.searchTerm.length == 0) {
			this.index();
		}
	}

};

const vm = new Vue({
	el:"#app",
	data:data,
	methods:methods,
	mounted(){
		this.index();
		this.scroll(this.explainedLocations);
	}
});