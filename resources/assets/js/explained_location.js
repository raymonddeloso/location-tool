import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

const urls = {
	index:'/api/de/explained-index',
	search:'/api/de/search-explained',
	undecided:'/api/de/undecide-locations',
	unexplained:'/api/de/unexplain-locations'
};

const data = {
	explainedLocations:[],
	count:0,
	next_page_url:"",
	prev_page_url:"",
	selectedKey:"",
	searchTerm:""
};

const methods = {

	index(){
		axios.get(urls.index)
		.then((response) => {
			this.explainedLocations = response.data.data;
			this.count = response.data.total;
			this.next_page_url = response.data.next_page_url;
			this.prev_page_url = response.data.prev_page_url;
		})
		.catch((error) => {
			console.log(error)
		})
	},
		
	locationType(location){
		if(location.is_sublocality){
			if(location.google_sublocality_level_2_long == null) {
				return "Sublocality Level 1";
			}
			return "Sublocality Level 2";
		}

		if(location.is_locality){
			return "Locality";
		}

		if(location.is_admin_area){
			if(location.google_admin_area_level_3_long == null &&
				location.google_admin_area_level_2_long == null) {
				return "Admin Area Level 1";
			}

			if(location.google_admin_area_level_3_long == null &&
				location.google_sublocality_level_2_long != null) {
				return "Admin Area Level 2";
			}

			if(location.google_admin_area_level_3_long != null) {
				return "Admin Area Level 3";
			}
		}
		
	},

	viewDetails(key){
		if(this.selectedKey.length == 0 || this.selectedKey != key){
			this.selectedKey = key;
		} else {
			this.selectedKey = "";
		}		
	},

	showDonutPostalCode(location){
		if(location.formatted_location_full.indexOf(" ") > -1){
			return location.formatted_location_full.split(" ")[0];
		} 
		return "";
	},

	scroll(explainedLocations){
		window.onscroll = () => {
			let bottomOfwindow = document.documentElement.scrollTop + window.innerHeight === document.documentElement.offsetHeight;

			if(bottomOfwindow) {
				axios.get(this.next_page_url)
				.then((response) => {
					for (let i = response.data.data.length - 1; i >= 0; i--) {
						this.explainedLocations.push(response.data.data[i]);
					}					
					this.next_page_url = response.data.next_page_url;
				})
			}
		}
	},

	search(){
		if(this.searchTerm.length >= 2) {
			axios.get(urls.search,{
				params:{
					term:this.searchTerm
				}
			})
			.then((response) => {
				console.log(response);
				this.explainedLocations = response.data.data;
				this.count = response.data.total;
				this.next_page_url = response.data.next_page_url;
			})
			.catch((error) => {
				console.log(error)
			})
		}

		if(this.searchTerm.length == 0) {
			this.index();
		}
	},

	moveToUndecided(location){
		Swal.fire({
		  title: 'Hey!',
		  text: "You are about to move this location to undecided!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, move it!'
		}).then((result) => {
		  if (result.value) {
		   	axios.post(urls.undecided,{
		   		id:location.raw_id
		   	})
		   	.then((response) => {
		   		Swal.fire('Location successfully moved to undecided!');
		   		this.index();
		   	})
		   	.catch((error) => {
		   		console.log(error);
		   	})
		  }
		})
	},

	moveToUnexplained(location){
		Swal.fire({
		  title: 'Hey!',
		  text: "You are about to remove this location!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, remove it!'
		}).then((result) => {
		  if (result.value) {
		   		axios.post(urls.unexplained,{
		   			id:location.raw_id
		   		})
		   		.then((response) => {
		   			Swal.fire('Location successfully removed!');
		   			this.index();
		   		})
		   		.catch((error) => {
		   			console.log(error);
		   		})
		  }
		})
	}


};

const vm = new Vue({
	el:"#app",
	data:data,
	methods:methods,
	mounted(){
		this.index();
		this.scroll(this.explainedLocations);

	}
});