<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCoResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('raw_locations', function (Blueprint $table) {
            $table->boolean('no_result')->default(0);
            $table->index('is_explained');
            $table->index('no_result');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('raw_locations', function (Blueprint $table) {
            $table->dropColumn('no_result');
        });
    }
}
