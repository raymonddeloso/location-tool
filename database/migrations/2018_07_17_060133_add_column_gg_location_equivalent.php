<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnGgLocationEquivalent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('raw_locations', function (Blueprint $table) {
            $table->integer('gg_loc_equivalent')->unsigned()->nullable();
            $table->index(['gg_loc_equivalent']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('raw_locations', function (Blueprint $table) {
            $table->dropColumn('gg_loc_equivalent');
        });
    }
}
