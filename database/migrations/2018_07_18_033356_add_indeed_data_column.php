<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndeedDataColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('google_location_responses', function (Blueprint $table) {
            $table->string('indeed_location_name_old')->nullable();
            $table->decimal('indeed_location_lat', 20, 12);
            $table->decimal('indeed_location_lng', 20, 12);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('google_location_responses', function (Blueprint $table) {
            $table->dropColumn('indeed_location_name_old');
            $table->dropColumn('indeed_location_lat');
            $table->dropColumn('indeed_location_lng');
        });
    }
}
