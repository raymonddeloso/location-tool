<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempDatabaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_databases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_count')->nullable();
            $table->string('city');
            $table->decimal('lat', 20, 12);
            $table->decimal('lng', 20, 12);
            $table->string('state')->nullable();
            $table->string('region')->nullable();
            $table->string('formatteddLocationFull')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_databases');
    }
}
