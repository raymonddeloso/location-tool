<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoogleLocationResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_location_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('raw_location_id')->nullable();
            $table->string("google_location_name");
            $table->string("google_location_place_id");
            $table->decimal("google_location_lat", 20, 12);
            $table->decimal("google_location_lng", 20, 12);
            $table->string("google_admin_area_level_1_long")->nullable();
            $table->string("google_admin_area_level_1_short")->nullable();
            $table->string("google_admin_area_level_2_long")->nullable();
            $table->string("google_admin_area_level_2_short")->nullable();
            $table->string("google_admin_area_level_3_long")->nullable();
            $table->string("google_admin_area_level_3_short")->nullable();
            $table->string("google_locality_long")->nullable();
            $table->string("google_locality_short")->nullable();
            $table->string("google_sublocality_level_1_long")->nullable();
            $table->string("google_sublocality_level_1_short")->nullable();
            $table->string("google_sublocality_level_2_long")->nullable();
            $table->string("google_sublocality_level_2_short")->nullable();
            $table->string("google_postal_code")->nullable();
            $table->decimal("haversine_distance", 20, 12);
            $table->boolean("is_sublocality")->default(0);
            $table->boolean("is_locality")->default(0);
            $table->boolean("is_admin_area")->default(0);
            $table->string("formatted_address");
            $table->boolean("sublocality_changed_to_locality")->default(0);
            $table->boolean("is_selected")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_location_responses');
    }
}
