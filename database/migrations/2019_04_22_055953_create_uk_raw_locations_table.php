<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUkRawLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uk_raw_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('indeed_location_name');
            $table->string('indeed_coordinates');
            $table->integer('job_count')->nullable();
            $table->string('formatted_location_full')->nullable();
            $table->string('state')->nullable();
            $table->string('region')->nullable();
            $table->boolean('is_explained')->default(0);
            $table->integer('gg_loc_equivalent');
            $table->decimal('gg_equivalent_haversine_distance', 20, 12);
            $table->boolean('undecided')->default(0);
            $table->boolean('no_result')->default(0);
            $table->string('explanation_type')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('batch_num')->nullable();
            $table->timestamps();
            $table->index('indeed_location_name', 'indeed_location_name');
            $table->index('indeed_coordinates', 'indeed_coordinates');
            $table->index('no_result', 'no_result');
            $table->index('is_explained', 'is_explained');
            $table->index('undecided', 'undecided');
            $table->index('gg_loc_equivalent', 'gg_loc_equivalent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uk_raw_locations');
    }
}
