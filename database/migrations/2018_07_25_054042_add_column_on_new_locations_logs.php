<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnNewLocationsLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_locations_logs', function (Blueprint $table) {
            $table->string('by_zip')->nullable();
            $table->string('by_name')->nullable();
            $table->string('by_manual')->nullable();
            $table->string('zero_result')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_locations_logs', function (Blueprint $table) {
            $table->dropColumn('by_zip');
            $table->dropColumn('by_name');
            $table->dropColumn('by_manual');
            $table->dropColumn('zero_result');
        });
    }
}
