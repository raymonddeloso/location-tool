<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInexColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('google_location_responses', function (Blueprint $table) {
            $table->index(['is_locality', 'is_sublocality', 'is_admin_area'], 'is_index');
            $table->index(['is_selected', 'google_location_name'], 'is_index_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('google_location_responses', function (Blueprint $table) {
            $table->dropIndex('is_index');
            $table->dropIndex('is_index_2');
        });
    }
}
