<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('indeed_location_name');
            $table->string('indeed_coordinates');
            $table->integer('job_count')->nullable();
            $table->string('formatted_location_full')->nullable();
            $table->string('state')->nullable();
            $table->string('region')->nullable();
            $table->boolean('is_explained')->default(0);
            $table->timestamps();
            $table->index(['indeed_coordinates', 'is_explained']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_locations');
    }
}
