<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewHaversineCloumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('raw_locations', function (Blueprint $table) {
            $table->decimal("gg_equivalent_haversine_distance", 20, 12);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('raw_locations', function (Blueprint $table) {
            $table->dropColumn("gg_equivalent_haversine_distance");
        });
    }
}
