<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('layout.master');
});

Route::get('/explained-location', ['as' => 'explained-location', 'uses' => 'RawLocationsController@showExplainedLocation']);

#Route::get('/unexplained-location', ['as' => 'unexplained-location', 'uses' => 'RawLocationsController@showUnexplainedLocation']);
Route::get('/unexplained-location', ['as' => 'unexplained-location', function () {return view('new_unexplained_location');}]);

Route::get('/undecided-location', ['as' => 'undecided-location', 'uses' => 'RawLocationsController@showUndecidedLocation']);

Route::get('/no-result', ['as' => 'no-result', 'uses' => 'RawLocationsController@showNoResult']);

Route::get('/crawl-logs', ['as' => 'crawling-logs', 'uses' => 'RawLocationsController@showCrawlingLogs']);

Route::get('/crawl-logs/{date}', ['as' => 'crawling-logs-by-date', 'uses' => 'RawLocationsController@showCrawlingLogsByDate']);

Route::get('/error-logs/', ['as' => 'error-log', 'uses' => 'RawLocationsController@showErrorLogs']);

Route::get('sub-localities', ['as' => 'sublocalities', 'uses' => 'RawLocationsController@getSublocalities']);

Route::get('/uk/explained-locations', ['as' => 'ukexplained', 'uses' => 'UKRawLocationsController@showExplainedView']);

Route::get('/uk/unexplained-locations', ['as' => 'ukunexplained', 'uses' => 'UKRawLocationsController@showUnexplainedView']);

Route::get('/uk/undecided-locations', ['as' => 'ukundecided', 'uses' => 'UKRawLocationsController@showUndecidedView']);

Route::get('/uk/not-crawled-locations', ['as' => 'notcrawled', 'uses' => 'UKRawLocationsController@showNotCrawledView']);
