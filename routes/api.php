<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('explian-locations', ['as' => 'explain-loc', 'uses' => 'RawLocationsController@explainLoc']);
Route::post('decide-locations', ['as' => 'decide-loc', 'uses' => 'RawLocationsController@decideLoc']);

Route::get('explianed-count', function (Request $request) {
    if ($request->ajax()) {
        return \App\RawLocation::where('is_explained', 1)->count();
    }
});

Route::get('/crawl-logs/explained/{date}', ['as' => 'explained-by-date', function (Request $request) {

    if ($request->ajax()) {

        $locations = \App\RawLocation::select('raw_locations.id as raw_id', 'raw_locations.*', 'google_location_responses.*')
            ->where('raw_locations.batch_num', $request->date)
            ->where('raw_locations.is_explained', 1)
            ->join('google_location_responses', 'google_location_responses.id', '=', 'raw_locations.gg_loc_equivalent')
            ->orderBy('indeed_location_name')
            ->paginate(10);

        $locations->setPath('/api/crawl-logs/explained/' . $request->date);

        return view('searchview', compact('locations'))->render();
    }
    return redirect('/crawl-logs/' . $request->date);
}]);

Route::get('/crawl-logs/unexplained/{date}', ['as' => 'unexplained-by-date', function (Request $request) {

    if ($request->ajax()) {

        $locations = \App\RawLocation::with('hasManyUrl')->where('is_explained', 0)
            ->where('batch_num', $request->date)
            ->where('undecided', 0)
            ->where('no_result', 0)
            ->orderBy('job_count', 'desc')
            ->paginate(10);

        $locations->setPath('/api/crawl-logs/unexplained/' . $request->date);

        return view('unexplained-ajax', compact('locations'))->render();
    }
    return redirect('/crawl-logs/' . $request->date);
}]);

Route::get('unexplained-location/index', 'RawLocationsController@showUnexplainedLocation');
Route::get('unexplained-location/google-addresses', 'RawLocationsController@getGoogleAddresses');
Route::get('unexplained-location/get-jobkey', 'RawLocationsController@getJobKeys');

Route::group(['prefix' => 'de'], function () {
    Route::get('explained-index', 'RawLocationsController@index');
    Route::get('search-explained', ['as' => 'search-explained', 'uses' => 'RawLocationsController@searchExplained']);
    Route::post('undecide-locations', ['as' => 'undecide-loc', 'uses' => 'RawLocationsController@undecideLoc']);
    Route::post('unexplain-locations', ['as' => 'remove-explained', 'uses' => 'RawLocationsController@removeExplained']);
});

Route::group(['prefix' => 'uk'], function () {
    Route::get('explained-index', 'UKRawLocationsController@index');
    Route::get('search-explained', 'UKRawLocationsController@search');
    Route::get('search-unexplained', 'UKRawLocationsController@searchUnexplained');
    Route::get('unexplained-index', 'UKRawLocationsController@unexplainedIndex');
    Route::get('unexplained-jobkeys', 'UKRawLocationsController@getJobKeys');
    Route::get('google-locations', 'UKRawLocationsController@googleLocations');
    Route::post('assign-loc', 'UKRawLocationsController@explainLoc');
    Route::post('undecide-loc', 'UKRawLocationsController@undecideLoc');
    Route::post('unexplain-loc', 'UKRawLocationsController@removeExplained');
    Route::get('undecided-index', 'UKRawLocationsController@undecidedIndex');
    Route::get('not-crawled', 'UKRawLocationsController@notCrawled');

});
