let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.js('resources/assets/js/new_unexplained_location.js','public/js')
	.js('resources/assets/js/explained_location.js','public/js')
	.js('resources/assets/js/uk/uk_explained.js','public/js')
	.js('resources/assets/js/uk/uk_unexplained.js','public/js')
	.js('resources/assets/js/uk/uk_undecided.js','public/js')
	.js('resources/assets/js/uk/uk_not_crawled.js','public/js')
   	.sass('resources/assets/sass/app.scss', 'public/css');


