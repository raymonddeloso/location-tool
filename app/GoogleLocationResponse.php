<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleLocationResponse extends Model
{
    protected $table = "google_location_responses";
}
