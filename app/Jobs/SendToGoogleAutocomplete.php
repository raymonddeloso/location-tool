<?php

namespace App\Jobs;

use App\Http\Controllers\LogController;
use App\Jobs\SendToPlaceDetailsApi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendToGoogleAutocomplete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * google place api key
     *
     */
    private $googleApiKey;

    /**
     * place type
     * [regions,cities]
     */
    private $type;

    /**
     * raw location object
     *
     */
    private $query;

    /**
     * LogController Object
     * @var [type]
     */
    private $log;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($query, $type)
    {
        $this->query        = $query;
        $this->type         = $type;
        $this->googleApiKey = env('GOOGLE_PLACES_API_1');
        $this->log          = new LogController;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->send();
    }

    public function send()
    {

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" . str_replace(" ", "%20", $this->query->city) .
            "&types=(" . $this->type . ")&language=de&components=country:de&key=" . $this->googleApiKey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => [
                "Cache-Control: no-cache",
            ],
        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {

            $this->log->store($this->query->city . "..." . $this->query->lat . "..." . $this->query->lng, $err);

        } else {

            $response = json_decode($response, true);

            $this->getResponse($response);

        }
    }

    public function getResponse($response)
    {
        if ($response['status'] != 'OK') {

            $this->log->store($this->query, json_encode($response));

        }

        if ($response['status'] === "ZERO_RESULTS") {

            // if response is zero result on regions type
            // send again as cities
            if ($this->type != "cities") {

                $this->sendCity($this->query->city);

            }

        }

        if (!is_null($response['predictions'])) {

            /**
             * loop through google results
             * and send to google place details api
             *
             */
            foreach ($response['predictions'] as $prediction) {
                $job = new SendToPlaceDetailsApi($this->query, $prediction['place_id'], env("GOOGLE_PLACES_API_2"));
                dispatch($job);
            }

        }
    }

    public function sendCity($locationQuery)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" . str_replace(" ", "%20", $locationQuery) .
            "&types=(cities)&language=de&components=country:de&key=" . $this->googleApiKey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => [
                "Cache-Control: no-cache",
            ],
        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {

            $this->log->store($this->query->city . "..." . $this->query->lat . "..." . $this->query->lng, $err);

        } else {

            $this->getResponse(json_encode($response, true));

        }

    }

}
