<?php

namespace App\Jobs;

use App\Http\Controllers\LogController;
use App\RawLocation;
use App\TmpGoogleResponse;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendToPlaceDetailsApi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * google place api key
     * @var [string]
     */
    private $googleApiKey;

    /**
     * google place id
     * @var [string]
     */
    private $placeId;

    /**
     * [$log description]
     * @var [type]
     */
    private $log;

    /**
     * raw location object
     * @var [type]
     */
    private $location;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($location, $placeId, $googleApiKey)
    {
        $this->placeId      = $placeId;
        $this->googleApiKey = $googleApiKey;
        $this->location     = $location;
        $this->log          = new LogController;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->send();
    }

    public function send()
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => "https://maps.googleapis.com/maps/api/place/details/json?placeid=" .
            $this->placeId . "&key=" . $this->googleApiKey . "&language=de",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => [
                "Cache-Control: no-cache",
            ],
        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->log->store($this->placeId, $err);
        } else {

            $response = json_decode($response, true);

            if ($response['status'] != 'OK') {
                $this->log->create($this->placeId, json_encode($response));
            } else {
                $this->processResponse($response);
            }

        }
    }

    public function processResponse($response)
    {
        $addressComp = $this->getAddressComponents($response);

        $addressComp['google_location_name']     = $response['result']['name'];
        $addressComp['google_location_place_id'] = $response['result']['place_id'];
        $addressComp['google_location_lat']      = $response['result']['geometry']['location']['lat'];
        $addressComp['google_location_lng']      = $response['result']['geometry']['location']['lng'];
        $addressComp['formatted_address']        = $response['result']['formatted_address'];
        $addressComp['indeed_location_name']     = $this->location->city;
        $addressComp['indeed_location_lat']      = $this->location->lat;
        $addressComp['indeed_location_lng']      = $this->location->lng;

        TmpGoogleResponse::insert($addressComp);

    }

    public function getAddressComponents($result)
    {
        $addressComp = [];

        foreach ($result['result']['address_components'] as $key => $value) {

            if (in_array('sublocality_level_2', $value['types'])) {
                $addressComp['google_sublocality_level_2_long']  = $value['long_name'];
                $addressComp['google_sublocality_level_2_short'] = $value['short_name'];
            }

            if (in_array('sublocality_level_1', $value['types'])) {
                $addressComp['google_sublocality_level_1_long']  = $value['long_name'];
                $addressComp['google_sublocality_level_1_short'] = $value['short_name'];
            }

            if (in_array('locality', $value['types'])) {
                $addressComp['google_locality_long']  = $value['long_name'];
                $addressComp['google_locality_short'] = $value['short_name'];
            }

            if (in_array('administrative_area_level_3', $value['types'])) {
                $addressComp['google_admin_area_level_3_long']  = $value['long_name'];
                $addressComp['google_admin_area_level_3_short'] = $value['short_name'];
            }

            if (in_array('administrative_area_level_2', $value['types'])) {
                $addressComp['google_admin_area_level_2_long']  = $value['long_name'];
                $addressComp['google_admin_area_level_2_short'] = $value['short_name'];
            }

            if (in_array('administrative_area_level_1', $value['types'])) {
                $addressComp['google_admin_area_level_1_long']  = $value['long_name'];
                $addressComp['google_admin_area_level_1_short'] = $value['short_name'];
            }

            if (in_array('postal_code', $value['types'])) {
                $addressComp['google_postal_code'] = $value['long_name'];
            }

        }

        return $addressComp;
    }
}
