<?php

namespace App\Console\Commands;

use App\RawLocation;
use Illuminate\Console\Command;

class RawLocations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'raw:location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->updateJobCount();
    }

    public function loadRaw()
    {
        $data = \DB::connection('ivan_database')
            ->table('indeed_jobs')
            ->get(['count(id) as job_count', 'city', 'lat', 'lon', 'state', 'region', 'formattedLocationFull']);

        $tmpArr = [];

        foreach ($data as $key => $d) {

            array_push($tmpArr, [
                'indeed_location_name'    => $d->city,
                'indeed_coordinates'      => (float) $d->lat . "," . (float) $d->lon,
                'job_count'               => $d->job_count,
                'formatted_location_full' => $d->formattedLocationFull,
                'state'                   => $d->state,
                'region'                  => $d->region,
                'zip_code'                => $this->extractZip($d->formattedLocationFull),

            ]);

        }
        \App\RawLocation::insert($tmpArr);
    }

    public function extractZip($string)
    {
        if (preg_match('/^[0-9]+\s/', $string)) {
            $zip = explode(" ", $string);
            return $zip[0];
        }

    }

    public function loadIvanDB()
    {
        \App\Location::chunk(1000, function ($data) {
            $tmp = [];

            foreach ($data as $location) {
                array_push($tmp, [
                    "indeed_location_name_old"         => $location->indeed_location_name,
                    "indeed_location_lat"              => (float) $location->indeed_location_lat,
                    "indeed_location_lng"              => (float) $location->indeed_location_lng,
                    "google_location_name"             => $location->google_location_name,
                    "google_location_place_id"         => $location->google_location_place_id,
                    "google_location_lat"              => (float) $location->google_location_lat,
                    "google_location_lng"              => (float) $location->google_location_lng,
                    "google_admin_area_level_1_long"   => $location->google_admin_area_level_1_long,
                    "google_admin_area_level_1_short"  => $location->google_admin_area_level_1_short,
                    "google_admin_area_level_2_long"   => $location->google_admin_area_level_2_long,
                    "google_admin_area_level_2_short"  => $location->google_admin_area_level_2_short,
                    "google_admin_area_level_3_long"   => $location->google_admin_area_level_3_long,
                    "google_admin_area_level_3_short"  => $location->google_admin_area_level_3_short,
                    "google_locality_long"             => $location->google_locality_long,
                    "google_locality_short"            => $location->google_locality_short,
                    "google_sublocality_level_1_long"  => $location->google_sublocality_level_1_long,
                    "google_sublocality_level_1_short" => $location->google_sublocality_level_1_short,
                    "google_sublocality_level_2_long"  => $location->google_sublocality_level_2_long,
                    "google_sublocality_level_2_short" => $location->google_sublocality_level_2_short,
                    "google_postal_code"               => $location->google_postal_code,
                    "haversine_distance"               => $location->haversine_distance,
                    "is_sublocality"                   => $location->is_sublocality,
                    "is_locality"                      => $location->is_locality,
                    "is_admin_area"                    => $location->is_admin_area,
                    "formatted_address"                => $location->formatted_address,
                    "sublocality_changed_to_locality"  => ($location->sublocality_changed_to_locality) ? $location->sublocality_changed_to_locality : 0,

                ]);
            }

            \App\GoogleLocationResponse::insert($tmp);
            $tmp = [];
        });
    }

    public function updateConnection()
    {
        $locs = \App\GoogleLocationResponse::whereNull('raw_location_id')->get();

        foreach ($locs as $loc) {

            $l = \App\RawLocation::where('indeed_location_name', '=', (float) $loc->indeed_location_name)
                ->where('indeed_coordinates', '=', (float) $loc->indeed_location_lat . ',' . (float) $loc->indeed_location_lng)
                ->first();

            $tmp = explode(",", $l->indeed_coordinates);

            $s = \App\GoogleLocationResponse::where('indeed_location_lat', $tmp[0])
                ->where('indeed_location_lng', $tmp[1])
                ->get(['id'])->toArray();

            \App\GoogleLocationResponse::whereIn('id', array_map(function ($aa) {return $aa['id'];}, $s))
                ->update(['raw_location_id' => $l->id]);

        }
        /*$loc = \App\RawLocation::chunk(1000, function ($data) {

    foreach ($data as $value) {
    $tmp = explode(",", $value->indeed_coordinates);

    $l = \App\GoogleLocationResponse::where('indeed_location_name', '=', $value->indeed_location_name)
    ->where('indeed_location_lat', '=', $tmp[0])
    ->where('indeed_location_lng', '=', $tmp[1])
    ->get(['id'])->toArray();

    if (count($l) == 0) {
    continue;
    }

    $ids = array_map(function ($i) {
    return $i['id'];
    }, $l);

    \App\GoogleLocationResponse::whereIn('id', $ids)->update(['raw_location_id' => $value->id]);

    }

    });*/
    }

    public function explained()
    {
        $x = \DB::connection('ivan_database')
            ->table('zip_matches_from_raw')
            ->get(['indeed_location_name', 'indeed_lat', 'indeed_lon', 'google_name', 'google_lat', 'google_lon', 'haversine_distance', 'result_from']);

        foreach ($x as $key => $newx) {

            echo $key . "\n";
            $ggLoc = \App\GoogleLocationResponse::where('google_location_name', '=', $newx->google_name)
                ->where('google_location_lat', '=', (float) $newx->google_lat)
                ->where('google_location_lng', '=', (float) $newx->google_lon)
                ->first();

            $rawLoc = \App\RawLocation::where('indeed_location_name', '=', $newx->indeed_location_name)
                ->where('indeed_coordinates', '=', (float) $newx->indeed_lat . ',' . (float) $newx->indeed_lon)
                ->first();

            $rawLoc->is_explained                     = 1;
            $rawLoc->gg_loc_equivalent                = $ggLoc->id;
            $rawLoc->gg_equivalent_haversine_distance = $newx->haversine_distance;
            $rawLoc->explanation_type                 = $newx->result_from;
            $rawLoc->save();
            //->update(['is_explained' => 1, 'gg_loc_equivalent' => $ggLoc->id]);

        }
    }

    public function updateZip()
    {
        $locs = \App\RawLocation::get();
        foreach ($locs as $key => $loc) {
            echo $key . "\n";
            $zipLoc = \DB::connection('ivan_database')
                ->table('indeed_jobs')
                ->where('city', $loc->indeed_location_name)
                ->where('lat', explode(",", $loc->indeed_coordinates)[0])
                ->where('lon', explode(",", $loc->indeed_coordinates)[1])
                ->first();
            if (is_null($zipLoc)) {
                continue;
            }
            $loc->formatted_location_full = $zipLoc->formattedLocationFull;
            $loc->state                   = $zipLoc->state;
            $loc->region                  = $zipLoc->region;
            $loc->save();

        }
    }

    public function indetifyNoResult()
    {
        $locs = \DB::connection('ivan_database')
            ->table('no_matches_from_raw')
            ->where('reason', 'no_result')->get();

        foreach ($locs as $key => $loc) {
            echo $key . "\n";
            \App\RawLocation::where('indeed_location_name', $loc->indeed_location_name)
                ->where('indeed_coordinates', $loc->indeed_lat . ',' . $loc->indeed_lon)
                ->update(['no_result' => 1]);

        }
    }

    public function updateLinks()
    {
        $locations = \App\RawLocation::select('id', 'indeed_location_name', 'indeed_coordinates')
            ->chunk(200, function ($datas) {
                $tmpArr = [];
                foreach ($datas as $data) {
                    $urls = \DB::connection('ivan_database')
                        ->table('indeed_jobs_url')
                        ->where('city', $data->indeed_location_name)
                        ->where('lat', explode(",", $data->indeed_coordinates)[0])
                        ->where('lon', explode(",", $data->indeed_coordinates)[1])
                        ->orderBy('date_tagged', 'desc')
                        ->take(5)
                        ->get(['url', 'date_tagged']);

                    foreach ($urls as $url) {
                        $link = explode("&qd", $url->url);

                        array_push($tmpArr, [
                            'raw_location_id' => $data->id,
                            'url'             => $link[0],
                        ]);
                    }

                }
                \App\RawLocationUrl::insert($tmpArr);
            });
    }

    public function updateAdminArea3()
    {
        $tmpArr = [];

        $locs = \DB::connection('ivan_database')
            ->table('donut_gg_locations_de_cities_071818')
            ->get();

        foreach ($locs as $key => $loc) {
            array_push($tmpArr, [
                "indeed_location_name_old"         => $loc->indeed_location_name,
                "indeed_location_lat"              => $loc->indeed_location_lat,
                "indeed_location_lng"              => $loc->indeed_location_lng,
                "google_location_name"             => $loc->google_location_name,
                "google_location_place_id"         => $loc->google_location_place_id,
                "google_location_lat"              => $loc->google_location_lat,
                "google_location_lng"              => $loc->google_location_lng,
                "google_admin_area_level_1_long"   => $loc->google_admin_area_level_1_long,
                "google_admin_area_level_1_short"  => $loc->google_admin_area_level_1_short,
                "google_admin_area_level_2_long"   => $loc->google_admin_area_level_2_long,
                "google_admin_area_level_2_short"  => $loc->google_admin_area_level_2_short,
                "google_admin_area_level_3_long"   => $loc->google_admin_area_level_3_long,
                "google_admin_area_level_3_short"  => $loc->google_admin_area_level_3_short,
                "google_locality_long"             => $loc->google_locality_long,
                "google_locality_short"            => $loc->google_locality_short,
                "google_sublocality_level_1_long"  => $loc->google_sublocality_level_1_long,
                "google_sublocality_level_1_short" => $loc->google_sublocality_level_1_short,
                "google_sublocality_level_2_long"  => $loc->google_sublocality_level_2_long,
                "google_sublocality_level_2_short" => $loc->google_sublocality_level_2_short,
                "google_postal_code"               => $loc->google_postal_code,
                "haversine_distance"               => $loc->haversine_distance,
                "is_sublocality"                   => $loc->is_sublocality,
                "is_locality"                      => $loc->is_locality,
                "is_admin_area"                    => $loc->is_admin_area,
                "formatted_address"                => $loc->formatted_address,
            ]);
        }

        \App\GoogleLocationResponse::insert($tmpArr);

    }

    public function updateMatchesAdminArea3()
    {
        $locations = \DB::connection('ivan_database')
            ->table('donut_location_matches_cities')
            ->get();

        foreach ($locations as $location) {

            $googleLoc = \App\GoogleLocationResponse::where('google_location_name', $location->gg_name)
                ->where('google_location_lat', (float) $location->gg_lat)
                ->where('google_location_lng', (float) $location->gg_lon)
                ->first();
            if (is_null($googleLoc)) {
                dd($location, (float) $location->gg_lat, (float) $location->gg_lon);
            }
            $rawLoc = \App\RawLocation::where('indeed_location_name', $location->donut_name)
                ->where('indeed_coordinates', $location->donut_lat . ',' . $location->donut_lon)->first();

            $rawLoc->is_explained                     = 1;
            $rawLoc->gg_loc_equivalent                = $googleLoc->id;
            $rawLoc->gg_equivalent_haversine_distance = $googleLoc->haversine_distance;
            $rawLoc->no_result                        = 0;
            $rawLoc->save();

        }
    }

    public function sendToGoogleAutocomplete()
    {

        /*$curl = curl_init();

        curl_setopt_array($curl, [
        CURLOPT_URL            => "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=stutgart&types=%28regions%29&language=de&components=country:de&key=AIzaSyC4IBfNkZx2JbPpP7picW9t44yka26HNvA",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => "GET",
        CURLOPT_HTTPHEADER     => [
        "Cache-Control: no-cache",
        ],
        ]);*/

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL            => "https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJ04-twTTbmUcR5M-RdxzB1Xk&key=AIzaSyC4IBfNkZx2JbPpP7picW9t44yka26HNvA&language=de",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => [
                "Cache-Control: no-cache",
            ],
        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            dd($response);
        }
    }

    public function updateNoResult()
    {
        $locs = \App\RawLocation::where('no_result', 1)
            ->get();

        foreach ($locs as $key => $value) {

            $count = \App\GoogleLocationResponse::where('indeed_location_name_old', $value->indeed_location_name)->count();

            if ($count > 0) {
                echo $key . "\n";
                \App\RawLocation::where('id', $value->id)->update(['no_result' => 0]);
            }
        }
    }

    public function checkHaversine()
    {
        $googles = \App\GoogleLocationResponse::where('indeed_location_name_old', "Zwiesel")->get();

        foreach ($googles as $google) {
            if ($google->google_location_name === "Zwiesel") {
                dd($google->google_location_name . " " . calculateHaversine("49.0123,13.2256", $google->google_location_lat, $google->google_location_lng) . "\n");
            }
            echo $google->google_location_name . " " . calculateHaversine("49.0123,13.2256", $google->google_location_lat, $google->google_location_lng) . "\n";
        }

    }

    public function sortHav()
    {
        $res = [
            "0.20894373658108...11",
            "3.5356795032199...12",
            "2.5044634786806...13",
            "3.8908232909826...14",
        ];
        asort($res);
        dd($res);
    }

    public function crossCheck()
    {
        $locs = \App\RawLocation::where('no_result', 1)->get();

        $count = 0;

        foreach ($locs as $key => $value) {
            $old_loc = \App\GoogleLocationResponse::where('indeed_location_name_old', $value->indeed_location_name)
                ->where('indeed_location_lat', explode(",", $value->indeed_coordinates)[0])
                ->where('indeed_location_lng', explode(",", $value->indeed_coordinates)[1])
                ->count();

            if ($old_loc > 0) {
                echo $value->id . ",";
                $count += 1;

            }

        }
        dd($count);
    }

    public function sendToAutocomplete()
    {
        $locs = \App\RawLocation::where('no_result', 1)->get();

        foreach ($locs as $key => $rawlocvalue) {

            echo $key . "\n";

            $response = $this->autocomplete($rawlocvalue->indeed_location_name);

            if ($response['status'] === "ZERO_RESULTS" || is_null($response)) {

                $response = $this->autocomplete($rawlocvalue->indeed_location_name, "cities");

                if (!is_null($response['predictions'])) {

                    foreach ($response['predictions'] as $key => $value) {
                        $placeDetails = $this->placeDetails($value['place_id']);

                        $addressComp                             = $this->getAddressComponents($placeDetails);
                        $addressComp['google_location_name']     = $placeDetails['result']['name'];
                        $addressComp['google_location_place_id'] = $placeDetails['result']['place_id'];
                        $addressComp['google_location_lat']      = $placeDetails['result']['geometry']['location']['lat'];
                        $addressComp['google_location_lng']      = $placeDetails['result']['geometry']['location']['lng'];
                        $addressComp['formatted_address']        = $placeDetails['result']['formatted_address'];
                        $addressComp['indeed_location_name_old'] = $rawlocvalue->indeed_location_name;
                        $addressComp['indeed_location_lat']      = explode(",", $rawlocvalue->indeed_coordinates)[0];
                        $addressComp['indeed_location_lng']      = explode(",", $rawlocvalue->indeed_coordinates)[1];

                        \App\GoogleLocationResponse::insert($addressComp);
                        echo "Added " . $rawlocvalue->indeed_location_name;
                    }
                }

                if ($response['status'] === "ZERO_RESULTS") {
                    echo "No Result " . $rawlocvalue->indeed_location_name . "\n";
                }

                continue;
            } else {
                foreach ($response['predictions'] as $key => $value) {
                    $placeDetails = $this->placeDetails($value['place_id']);

                    if ($placeDetails['status'] === "NOT_FOUND") {
                        echo "Place Details not found " . $rawlocvalue->indeed_location_name . "\n";
                        continue;
                    }

                    $addressComp                             = $this->getAddressComponents($placeDetails);
                    $addressComp['google_location_name']     = $placeDetails['result']['name'];
                    $addressComp['google_location_place_id'] = $placeDetails['result']['place_id'];
                    $addressComp['google_location_lat']      = $placeDetails['result']['geometry']['location']['lat'];
                    $addressComp['google_location_lng']      = $placeDetails['result']['geometry']['location']['lng'];
                    $addressComp['formatted_address']        = $placeDetails['result']['formatted_address'];
                    $addressComp['indeed_location_name_old'] = $rawlocvalue->indeed_location_name;
                    $addressComp['indeed_location_lat']      = explode(",", $rawlocvalue->indeed_coordinates)[0];
                    $addressComp['indeed_location_lng']      = explode(",", $rawlocvalue->indeed_coordinates)[1];

                    \App\GoogleLocationResponse::insert($addressComp);
                }

            }

        }

    }

    public function autocomplete($query, $type = "regions")
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" . str_replace(" ", "%20", $query) . "&types=(" . $type . ")&language=de&components=country:de&key=AIzaSyAl8OenKbNHXCM2QoXafJV4DWlD1tEeiUM",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 60,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",

        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            dd($err);
        }

        $response = json_decode($response, true);

        return $response;
    }

    public function placeDetails($placeid)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => "https://maps.googleapis.com/maps/api/place/details/json?placeid=" .
            $placeid
            . "&key=AIzaSyAp3leeouxPxme4IaP2iOLRGwV5_jJ2XUw&language=de",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",

        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);
    }

    public function getAddressComponents($result)
    {
        $addressComp = [];

        foreach ($result['result']['address_components'] as $key => $value) {

            if (in_array('sublocality_level_2', $value['types'])) {
                $addressComp['google_sublocality_level_2_long']  = $value['long_name'];
                $addressComp['google_sublocality_level_2_short'] = $value['short_name'];
            }

            if (in_array('sublocality_level_1', $value['types'])) {
                $addressComp['google_sublocality_level_1_long']  = $value['long_name'];
                $addressComp['google_sublocality_level_1_short'] = $value['short_name'];
            }

            if (in_array('locality', $value['types'])) {
                $addressComp['google_locality_long']  = $value['long_name'];
                $addressComp['google_locality_short'] = $value['short_name'];
            }

            if (in_array('administrative_area_level_3', $value['types'])) {
                $addressComp['google_admin_area_level_3_long']  = $value['long_name'];
                $addressComp['google_admin_area_level_3_short'] = $value['short_name'];
            }

            if (in_array('administrative_area_level_2', $value['types'])) {
                $addressComp['google_admin_area_level_2_long']  = $value['long_name'];
                $addressComp['google_admin_area_level_2_short'] = $value['short_name'];
            }

            if (in_array('administrative_area_level_1', $value['types'])) {
                $addressComp['google_admin_area_level_1_long']  = $value['long_name'];
                $addressComp['google_admin_area_level_1_short'] = $value['short_name'];
            }

            if (in_array('postal_code', $value['types'])) {
                $addressComp['google_postal_code'] = $value['long_name'];
            }

        }

        return $addressComp;

    }

    public function applyRule($indeedLocation)
    {

        $indeedZipCode = extractZip($indeedLocation->zip_code);

        $googleResponses = \App\GoogleLocationResponse::where('indeed_location_name_old', $indeedLocation->indeed_location_name)->get();

        $zipCodeMatchHaversine = [];

        $nameMatchHaversine = [];

        foreach ($googleResponses as $googleResponse) {

            //check of indeed location has a zip code
            if (!is_null($indeedZipCode)) {
                echo "not null indeed zip code. \n";

                //check if indeed location zip code == google zip code
                if ($indeedZipCode === $googleResponse->google_postal_code) {
                    echo "Checking haversine exact match zip code ." . $indeedLocation->indeed_location_name . " " . $indeedLocation->indeed_coordinates . "\n";
                    //check if indeed location = locality or sublocality
                    if ($indeedLocation->indeed_location_name === $googleResponse->google_sublocality_level_2_long ||
                        $indeedLocation->indeed_location_name === $googleResponse->google_sublocality_level_1_long ||
                        $indeedLocation->indeed_location_name === $googleResponse->google_locality_long) {

                        $hav_distance = calculateHaversine($indeedLocation->indeed_coordinates,
                            (float) $googleResponse->google_location_lat, (float) $googleResponse->google_location_lng);

                        //check if haversine distance <= 8
                        if ($hav_distance <= 8) {
                            array_push($zipCodeMatchHaversine, $hav_distance . "..." . $googleResponse->id . "..." . $indeedLocation->id);
                        }

                    }

                }

            }

            /**
             * if zip is not match, check if exact match on name
             * and if haversine is <= 8
             */
            if ($indeedLocation->indeed_location_name === $googleResponse->google_location_name) {

                echo "Exact match indeed and google location name " . $indeedLocation->indeed_location_name . "\n";

                $hav_distance = calculateHaversine($indeedLocation->indeed_coordinates,
                    (float) $googleResponse->google_location_lat, (float) $googleResponse->google_location_lng);

                if ($hav_distance <= 8) {
                    array_push($nameMatchHaversine, $hav_distance . "..." . $googleResponse->id . "..." . $indeedLocation->id);
                }

            }

        } //end foreach

        if (count($zipCodeMatchHaversine) > 0) {

            asort($zipCodeMatchHaversine);

            $data = explode("...", $zipCodeMatchHaversine[0]);

            $rawloc                                   = \App\RawLocation::find($data[2]);
            $rawloc->is_explained                     = 1;
            $rawloc->gg_loc_equivalent                = $data[1];
            $rawloc->gg_equivalent_haversine_distance = $data[0];
            $rawloc->explanation_type                 = 'zip';
            $rawloc->save();

            //add and assign new indeed_location
            //continue;
        }

        if (count($zipCodeMatchHaversine) == 0 && count($nameMatchHaversine) > 0) {

            asort($nameMatchHaversine);

            $data = explode("...", $nameMatchHaversine[0]);

            $rawloc                                   = \App\RawLocation::find($data[2]);
            $rawloc->is_explained                     = 1;
            $rawloc->gg_loc_equivalent                = $data[1];
            $rawloc->gg_equivalent_haversine_distance = $data[0];
            $rawloc->explanation_type                 = 'name';
            $rawloc->save();

            //add and assign new indeed_location
        }
        if (count($zipCodeMatchHaversine) == 0 && count($nameMatchHaversine) == 0) {

        }

        //add new indeed location unexplained with result

    }

    public function updateJobCounst()
    {
        $rawlocs = RawLocation::get();

        foreach ($rawlocs as $value) {

            $prodData = \DB::connection('ivan_database')
                ->table('indeed_jobs')
                ->where('city', $value->indeed_location_name)
                ->where('lat', explode(",", $value->indeed_coordinates)[0])
                ->where('lon', explode(",", $value->indeed_coordinates)[1])
                ->get(['id']);

            if ($prodData->count() > 0) {

                $loc = RawLocation::find($value->id);

                $loc->job_count = $prodData->count();
                $loc->save();

            }

        }

    }

	public function updateJobCount()
    {
        $rawLocations = RawLocation::where('is_explained', 0)->get();

        foreach ($rawLocations as $key => $rawLocation) {
		echo $key."\n";
            $jobCount = \DB::connection('crawler_server')
                ->table('indeed_jobs')
                ->select(\DB::raw('count(id) as job_count'))
                ->where('city', $rawLocation->indeed_location_name)
                ->where('lat', explode(",", $rawLocation->indeed_coordinates)[0])
                ->where('lon', explode(",", $rawLocation->indeed_coordinates)[1])
                ->groupBy('city')
                ->groupBy('lat')
                ->groupBy('lon')
                ->first();
		if(!is_null($jobCount)){
            RawLocation::where('id', $rawLocation->id)
                ->update(['job_count' => $jobCount->job_count]);
		}
        }

    }
}
