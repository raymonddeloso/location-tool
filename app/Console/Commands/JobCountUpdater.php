<?php

namespace App\Console\Commands;

use App\RawLocation;
use Carbon\Carbon;
use Illuminate\Console\Command;

class JobCountUpdater extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobcount:updater';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getJobs();
    }

    public function getJobs()
    {
        $locations = RawLocation::where('is_explained', 0)
            ->select('indeed_location_name')
            ->where('undecided', 0)
            ->where('no_result', 0)
            ->groupBy('indeed_location_name')
            ->chunk(500, function ($query) {
                $locations = array_map(function ($item) {return $item['indeed_location_name'];}, $query->toArray());

                $jobs = \DB::connection('crawler_server')
                    ->table('indeed_jobs')
                    ->select(\DB::raw('count(id) as job_count,city,lat,lon'))
                    ->whereIn('city', $locations)
                    ->where('date_tagged', '>=', Carbon::now()->subDays(60)->toDateString())
                    ->groupBy('city')
                    ->groupBy('lat')
                    ->groupBy('lon')
                    ->get();

                foreach ($jobs as $key => $value) {
                    RawLocation::where('indeed_location_name', $value->city)
                        ->where('indeed_coordinates', $value->lat . "," . $value->lon)
                        ->update(['job_count' => $value->job_count]);
                }

            });

    }
}
