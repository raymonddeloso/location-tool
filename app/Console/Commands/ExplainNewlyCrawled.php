<?php

namespace App\Console\Commands;

use App\GoogleLocationResponse;
use App\RawLocation;
use App\TempDatabase;
use App\TmpGoogleResponse;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExplainNewlyCrawled extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'explain:newlycrawled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->moveNewLocations();
        echo "Locations Moved!\n";
        $this->moveNewGoogleResponses();
        echo "Google Responses Moved!\n";
        $this->explainLocation();
	sleep(3);
	TempDatabase::truncate();
	TmpGoogleResponse::truncate();
    }

    public function applyRule()
    {

    }

    public function moveNewLocations()
    {
        $newLocations = TempDatabase::get();

        $toBeAddedNewLocations = [];

        foreach ($newLocations as $newLocation) {

            array_push($toBeAddedNewLocations, [
                'indeed_location_name'    => $newLocation->city,
                'indeed_coordinates'      => (float) $newLocation->lat . ',' . (float) $newLocation->lng,
                'job_count'               => $newLocation->job_count,
                'formatted_location_full' => $newLocation->formatteddLocationFull,
                'state'                   => $newLocation->state,
                'region'                  => $newLocation->region,
                'created_at'              => $newLocation->created_at,
                'updated_at'              => $newLocation->updated_at,
                'zip_code'                => $newLocation->zip_code,
                'batch_num'               => Carbon::now()->subDay()->toDateString(),

            ]);
        }

        RawLocation::insert($toBeAddedNewLocations);
    }

    public function moveNewGoogleResponses()
    {
        $newGoogleResponses = TmpGoogleResponse::get();

        $toBeAddedNewLocations = [];

        foreach ($newGoogleResponses as $newGoogleResponse) {
	
            $locationType = $this->locationType($newGoogleResponse);

            array_push($toBeAddedNewLocations, [
                "google_location_name"             => $newGoogleResponse->google_location_name,
                "google_location_place_id"         => $newGoogleResponse->google_location_place_id,
                "google_location_lat"              => $newGoogleResponse->google_location_lat,
                "google_location_lng"              => $newGoogleResponse->google_location_lng,
                "google_admin_area_level_1_long"   => $newGoogleResponse->google_admin_area_level_1_long,
                "google_admin_area_level_1_short"  => $newGoogleResponse->google_admin_area_level_1_short,
                "google_admin_area_level_2_long"   => $newGoogleResponse->google_admin_area_level_2_long,
                "google_admin_area_level_2_short"  => $newGoogleResponse->google_admin_area_level_2_short,
                "google_admin_area_level_3_long"   => $newGoogleResponse->google_admin_area_level_3_long,
                "google_admin_area_level_3_short"  => $newGoogleResponse->google_admin_area_level_3_short,
                "google_locality_long"             => $newGoogleResponse->google_locality_long,
                "google_locality_short"            => $newGoogleResponse->google_locality_short,
                "google_sublocality_level_1_long"  => $newGoogleResponse->google_sublocality_level_1_long,
                "google_sublocality_level_1_short" => $newGoogleResponse->google_sublocality_level_1_short,
                "google_sublocality_level_2_long"  => $newGoogleResponse->google_sublocality_level_2_long,
                "google_sublocality_level_2_short" => $newGoogleResponse->google_sublocality_level_2_short,
                "google_postal_code"               => $newGoogleResponse->google_postal_code,
                "haversine_distance"               => $newGoogleResponse->haversine_distance,
                "is_sublocality"                   => ($locationType === "sublocality") ? 1 : 0,
                "is_locality"                      => ($locationType === "locality") ? 1 : 0,
                "is_admin_area"                    => ($locationType === "admin") ? 1 : 0,
                "formatted_address"                => $newGoogleResponse->formatted_address,
                "sublocality_changed_to_locality"  => $newGoogleResponse->sublocality_changed_to_locality,
                "is_selected"                      => $newGoogleResponse->is_selected,
                "created_at"                       => $newGoogleResponse->created_at,
                "updated_at"                       => $newGoogleResponse->updated_at,
                "indeed_location_name_old"         => $newGoogleResponse->indeed_location_name,
                "indeed_location_lat"              => $newGoogleResponse->indeed_location_lat,
                "indeed_location_lng"              => $newGoogleResponse->indeed_location_lng,
                "batch_num"                        => Carbon::now()->subDay()->toDateString(),

            ]);
        }

        GoogleLocationResponse::insert($toBeAddedNewLocations);
    }

    public function locationType($location)
    {
        if (!is_null($location->google_sublocality_level_2_long) ||
            !is_null($location->google_sublocality_level_1_long)) {
            return "sublocality";
        }

        if (!is_null($location->google_locality_long) &&
            is_null($location->google_sublocality_level_2_long) &&
            is_null($location->google_sublocality_level_1_long)) {
            return "locality";
        }

        if (!is_null($location->google_admin_area_level_1_long) ||
            !is_null($location->google_admin_area_level_2_long) ||
            !is_null($location->google_admin_area_level_3_long) && is_null($loaction->google_locality_long)) {
            return "admin";
        }
    }

    public function explainLocation()
    {
        $rawlocations = RawLocation::where('batch_num', Carbon::now()->subDay()->toDateString())
            ->where('is_explained', 0)
            ->get();

        foreach ($rawlocations as $value) {

            $indeedZipCode = $value->zip_code;

            $googleResponses = GoogleLocationResponse::where('indeed_location_name_old', $value->indeed_location_name)->get();

            $zipCodeMatchHaversine = [];

            $nameMatchHaversine = [];

            foreach ($googleResponses as $googleResponse) {
                echo $value->indeed_location_name . " " . $googleResponse->google_location_name . "\n";
                //check of indeed location has a zip code
                if (!is_null($indeedZipCode)) {
                    echo "not null indeed zip code. \n";

                    //check if indeed location zip code == google zip code
                    if ($indeedZipCode === $googleResponse->google_postal_code) {

                        //check if indeed location = locality or sublocality
                        if ($value->indeed_location_name === $googleResponse->google_sublocality_level_2_long ||
                            $value->indeed_location_name === $googleResponse->google_sublocality_level_1_long ||
                            $value->indeed_location_name === $googleResponse->google_locality_long) {

                            $hav_distance = calculateHaversine($value->indeed_coordinates,
                                (float) $googleResponse->google_location_lat, (float) $googleResponse->google_location_lng);

                            //check if haversine distance <= 8
                            if ($hav_distance <= 8) {
                                array_push($zipCodeMatchHaversine, $hav_distance . "..." . $googleResponse->id . "..." . $value->id);
                            }

                        }

                    }

                }

                /**
                 * if zip is not match, check if exact match on name
                 * and if haversine is <= 8
                 */
                if ($value->indeed_location_name === $googleResponse->google_location_name) {

                    $hav_distance = calculateHaversine($value->indeed_coordinates,
                        (float) $googleResponse->google_location_lat, (float) $googleResponse->google_location_lng);

                    if ($hav_distance <= 8) {
                        array_push($nameMatchHaversine, $hav_distance . "..." . $googleResponse->id . "..." . $value->id);
                    }

                }

            } //end foreach google responses

            if (count($zipCodeMatchHaversine) > 0) {

                asort($zipCodeMatchHaversine);

                $data = explode("...", $zipCodeMatchHaversine[0]);

                $rawloc                                   = \App\RawLocation::find($data[2]);
                $rawloc->is_explained                     = 1;
                $rawloc->gg_loc_equivalent                = $data[1];
                $rawloc->gg_equivalent_haversine_distance = $data[0];
                $rawloc->explanation_type                 = 'zip';
                $rawloc->save();
            }

            if (count($zipCodeMatchHaversine) == 0 && count($nameMatchHaversine) > 0) {

                asort($nameMatchHaversine);

                $data = explode("...", $nameMatchHaversine[0]);

                $rawloc                                   = \App\RawLocation::find($data[2]);
                $rawloc->is_explained                     = 1;
                $rawloc->gg_loc_equivalent                = $data[1];
                $rawloc->gg_equivalent_haversine_distance = $data[0];
                $rawloc->explanation_type                 = 'name';
                $rawloc->save();

                //add and assign new indeed_location
            }

            //add new indeed location unexplained with result

        } // end foreach rawlocations

    }

}
