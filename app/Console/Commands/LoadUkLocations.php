<?php

namespace App\Console\Commands;

use App\UkGoogleLocationResponse;
use App\UkRawLocation;
use Illuminate\Console\Command;

class LoadUkLocations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fromuk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->uploadLocationAssignment();
    }

    public function moveExplained()
    {
        $tmp       = [];
        $locations = \DB::connection('from_uk')
            ->table('uk_location_matches')
            ->get();

        foreach ($locations as $location) {
            array_push($tmp, [
                'indeed_location_name'             => $location->indeed_location,
                'indeed_coordinates'               => $location->indeed_lat . ',' . $location->indeed_lon,
                'is_explained'                     => $location->haversine_distance <= 8 ? 1 : 0,
                'gg_loc_equivalent'                => $location->gg_locations_id,
                'gg_equivalent_haversine_distance' => $location->haversine_distance,
                'no_result'                        => 0,
                'undecided'                        => 0,
            ]);

            if (count($tmp) % 1000 == 0) {
                UkRawLocation::insert($tmp);
                $tmp = [];
            }
        }
        UkRawLocation::insert($tmp);
    }

    public function moveGoogleResponses()
    {
        \DB::connection('from_uk')
            ->table('gg_locations')
            ->orderBy('id')
            ->chunk(1000, function ($data) {
                $tmp = [];
                foreach ($data as $location) {
                    array_push($tmp, [
                        'google_location_name'            => $location->location_name,
                        'google_location_place_id'        => $location->place_id,
                        'google_location_lat'             => $location->lat,
                        'google_location_lng'             => $location->lon,
                        'google_admin_area_level_1_long'  => $location->admin_area_level_1,
                        'google_admin_area_level_2_long'  => $location->admin_area_level_2,
                        'google_admin_area_level_3_long'  => $location->admin_area_level_3,
                        'google_locality_long'            => $location->locality,
                        'google_sublocality_level_1_long' => $location->sublocality_level_1,
                        'google_sublocality_level_2_long' => $location->sublocality_level_2,
                        'google_postal_code'              => $location->postal_code,
                        'haversine_distance'              => 0,
                        'google_postal_town'              => $location->postal_town,
                        'is_sublocality'                  => $location->is_sublocality,
                        'is_locality'                     => $location->is_locality,
                        'is_admin_area'                   => $location->is_admin_area,
                        'formatted_address'               => $location->formatted_address,
                        'sublocality_changed_to_locality' => 0,
                        'is_selected'                     => 0,
                        'indeed_location_name_old'        => $location->query_name,
                        'indeed_location_lat'             => 0,
                        'indeed_location_lng'             => 0,
                        'raw_response'                    => $location->raw,
                    ]);
                }
                UkGoogleLocationResponse::insert($tmp);
            });
    }

    public function loadNewUkLocations()
    {
        $locations = \File::get(storage_path('group_by_city_lat_lon.txt'));
        $locations = explode("\r\n", $locations);
        $tmp       = [];

        foreach ($locations as $loc) {
            $locData = explode("\t", str_replace('"', '', $loc));
            array_push($tmp, [
                'indeed_location_name'    => $locData[0],
                'indeed_coordinates'      => implode(",", [$locData[2], $locData[3]]),
                'job_count'               => $locData[7],
                'state'                   => $locData[4],
                'region'                  => $locData[1],
                'formatted_location_full' => $locData[6],
            ]);

            if ($tmp % 1000 == 0) {
                \DB::table('uk_raw_locations_copy')->insert($tmp);
                $tmp = [];
            }
        }
        \DB::table('uk_raw_locations_copy')->insert($tmp);
    }
    #select city, region, lat, lon, state, formattedLocation, formattedLocationFull, COUNT(*)

    public function uploadLocationAssignment()
    {
        $explainedLocations = \DB::table('uk_location_matches')
            ->select(\DB::raw('CONCAT(indeed_lat,",",indeed_lon) as indeed_coordinates, indeed_location,gg_locations_id,haversine_distance,match_type'))
            ->where('match_type', 'NO_MATCH')
            ->get();

        foreach ($explainedLocations as $key => $xlp) {
            echo $key . "\n";
            \DB::table('uk_raw_locations_copy')
                ->where('indeed_location_name', $xlp->indeed_location)
                ->where('indeed_coordinates', $xlp->indeed_coordinates)
                ->update([
                    'no_result' => 0,
                ]);
        }
    }
}
