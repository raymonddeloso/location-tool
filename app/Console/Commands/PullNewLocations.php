<?php

namespace App\Console\Commands;

use App\GoogleLocationResponse;
use App\Jobs\SendToGoogleAutocomplete;
use App\NewLocationsLog;
use App\RawLocation;
use App\TempDatabase;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PullNewLocations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commanddescription';

    private $newLocationsFound = 0;
    private $ruleAppliedZip    = 0;
    private $ruleAppliedName   = 0;
    private $ruleAppliedManual = 0;
    private $zeroResult        = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getLocations();
    }

    /**
     * Pull locations from crawler server
     * @return [type] [description]
     */
    public function getLocations()
    {

        $newLocations = \DB::connection('crawler_server')
            ->table('indeed_jobs')
            ->select(\DB::raw('count(id) as job_count, city, lat, lon, state, region, formattedLocationFull'))
            ->where('country', 'de')
            ->where('tagged',Carbon::now()->subDay()->toDateString())
            ->where('city', '!=', '')
            ->where('state', '!=', '')
            ->where('description', '!=', '')
            ->groupBy('city')
            ->groupBy('lat')
            ->groupBy('lon')
            ->get();

        echo "New Locations Count: " . $newLocations->count() . "\n";

        foreach ($newLocations as $pullLocationKey => $newLocation) {

            echo $pullLocationKey . "\n";

            if ($this->exists($newLocation) == 0) {
                $this->newLocationsFound += 1;

                $tmpData = new TempDatabase;

                $tmpData->job_count              = $newLocation->job_count;
                $tmpData->city                   = $newLocation->city;
                $tmpData->lat                    = $newLocation->lat;
                $tmpData->lng                    = $newLocation->lon;
                $tmpData->state                  = $newLocation->state;
                $tmpData->region                 = $newLocation->region;
                $tmpData->formatteddLocationFull = $newLocation->formattedLocationFull;
                $tmpData->zip_code               = extractZip($newLocation->formattedLocationFull);
                $tmpData->save();

                $job = new SendToGoogleAutocomplete($tmpData, "regions");
                $t   = dispatch($job);

            } /*else {

                //if the location has been sent to google already

                if ($this->checkIfQueriedToGoogle($newLocation->city) > 0 && $this->exists($newLocation) == 0) {

                    //save to new Raw Locations
                    $newRawLocation                          = new RawLocation;
                    $newRawLocation->indeed_location_name    = $newLocation->city;
                    $newRawLocation->indeed_coordinates      = $newLocation->lat . "," . $newLocation->lon;
                    $newRawLocation->job_count               = $newLocation->job_count;
                    $newRawLocation->state                   = $newLocation->state;
                    $newRawLocation->region                  = $newLocation->region;
                    $newRawLocation->formatted_location_full = $newLocation->formattedLocationFull;
                    $newRawLocation->zip_code                = extractZip($newLocation->formattedLocationFull);
                    $newRawLocation->batch_num               = '2018-08-04';//Carbon::now()->toDateString();
                    $newRawLocation->save();

                    $this->applyRule($newRawLocation);

                } else {
                    //new Location not sent to google
                    //save for crawling
                    echo "Saving Location for google Crawling. \n";

                    if ($this->existNoResult($newLocation) == 0) {
                        $noResultRawLoc                          = new RawLocation;
                        $noResultRawLoc->indeed_location_name    = $newLocation->city;
                        $noResultRawLoc->indeed_coordinates      = $newLocation->lat . "," . $newLocation->lon;
                        $noResultRawLoc->job_count               = $newLocation->job_count;
                        $noResultRawLoc->state                   = $newLocation->state;
                        $noResultRawLoc->region                  = $newLocation->region;
                        $noResultRawLoc->formatted_location_full = $newLocation->formattedLocationFull;
                        $noResultRawLoc->zip_code                = extractZip($newLocation->formattedLocationFull);
                        $noResultRawLoc->batch_num               = '2018-08-04';//Carbon::now()->toDateString();
                        $noResultRawLoc->no_result               = 1;
                       // $noResultRawLoc->save();
                    }

                    $this->zeroResult += 1;
                }

            }*/
        }

        echo $this->newLocationsFound . "\n";
        echo $this->ruleAppliedZip . "\n";
        echo $this->ruleAppliedName . "\n";
        echo $this->ruleAppliedManual . "\n";
        echo $this->zeroResult . "\n";

        //Log Pulled Locations data
        $newloc                        = new NewLocationsLog;
        $newloc->total_locations_count = $newLocations->count();
        $newloc->new_locations_found   = $this->newLocationsFound;
        $newloc->by_zip                = $this->ruleAppliedZip;
        $newloc->by_name               = $this->ruleAppliedName;
        $newloc->by_manual             = $this->ruleAppliedManual;
        $newloc->zero_result           = $this->zeroResult;
        $newloc->save();
    }

    /**
     * check if location (city, lat, lng)
     * already in the database
     *
     */
    public function exists($location)
    {
        return RawLocation::where('indeed_location_name', $location->city)
            ->where('indeed_coordinates', $location->lat . ',' . $location->lon)
            ->count();
    }

    public function existNoResult($location)
    {
        return RawLocation::where('indeed_location_name', $location->city)
            ->where('indeed_coordinates', $location->lat . ',' . $location->lon)
            ->where('no_result', 1)
            ->count();
    }

    /**
     * check if location (city) was
     * already sent to google places api
     *
     */
    public function checkIfQueriedToGoogle($location)
    {
        return GoogleLocationResponse::where('indeed_location_name_old', $location)->count();
    }

    /**
     * apply defined rules for matching locations
     *
     */
    public function applyRule($indeedLocation)
    {

        $indeedZipCode = $indeedLocation->zip_code;

        $googleResponses = GoogleLocationResponse::where('indeed_location_name_old', $indeedLocation->indeed_location_name)->get();

        $zipCodeMatchHaversine = [];
        $nameMatchHaversine    = [];

        foreach ($googleResponses as $googleResponse) {

            //check of indeed location has a zip code
            if (!is_null($indeedZipCode)) {
                echo "not null indeed zip code. \n";

                //check if indeed location zip code == google zip code
                if ($indeedZipCode === $googleResponse->google_postal_code) {

                    //check if indeed location = locality or sublocality
                    if ($indeedLocation->indeed_location_name === $googleResponse->google_sublocality_level_2_long ||
                        $indeedLocation->indeed_location_name === $googleResponse->google_sublocality_level_1_long ||
                        $indeedLocation->indeed_location_name === $googleResponse->google_locality_long) {

                        $hav_distance = calculateHaversine($indeedLocation->indeed_coordinates,
                            (float) $googleResponse->google_location_lat, (float) $googleResponse->google_location_lng);

                        //check if haversine distance <= 8
                        if ($hav_distance <= 8) {
                            array_push($zipCodeMatchHaversine, $hav_distance . "..." . $googleResponse->id . "..." . $indeedLocation->id);
                        }

                    }

                }

            }

            /**
             * if zip is not match, check if exact match on name
             * and if haversine is <= 8
             */
            if ($indeedLocation->indeed_location_name === $googleResponse->google_location_name) {

                $hav_distance = calculateHaversine($indeedLocation->indeed_coordinates,
                    (float) $googleResponse->google_location_lat, (float) $googleResponse->google_location_lng);

                if ($hav_distance <= 8) {
                    array_push($nameMatchHaversine, $hav_distance . "..." . $googleResponse->id . "..." . $indeedLocation->id);
                }

            }

        } //end foreach

        if (count($zipCodeMatchHaversine) > 0) {

            $this->ruleAppliedZip += 1;
            asort($zipCodeMatchHaversine);

            $data = explode("...", $zipCodeMatchHaversine[0]);

            $rawloc                                   = \App\RawLocation::find($data[2]);
            $rawloc->is_explained                     = 1;
            $rawloc->gg_loc_equivalent                = $data[1];
            $rawloc->gg_equivalent_haversine_distance = $data[0];
            $rawloc->explanation_type                 = 'zip';
            $rawloc->save();
        }

        if (count($zipCodeMatchHaversine) == 0 && count($nameMatchHaversine) > 0) {

            $this->ruleAppliedName += 1;
            asort($nameMatchHaversine);

            $data = explode("...", $nameMatchHaversine[0]);

            $rawloc                                   = \App\RawLocation::find($data[2]);
            $rawloc->is_explained                     = 1;
            $rawloc->gg_loc_equivalent                = $data[1];
            $rawloc->gg_equivalent_haversine_distance = $data[0];
            $rawloc->explanation_type                 = 'name';
            $rawloc->save();

            //add and assign new indeed_location
        }

        //add new indeed location unexplained with result
        if (count($zipCodeMatchHaversine) == 0 && count($nameMatchHaversine) == 0) {
            $this->ruleAppliedManual += 1;
        }
    }

}
