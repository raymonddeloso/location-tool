<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RawLocation extends Model
{
    protected $table = "raw_locations";

    public function hasManyGoogleResponse()
    {
        return $this->hasMany(GoogleLocationResponse::class, 'raw_location_id');
    }

    public function hasManyUrl()
    {
        return $this->hasMany(RawLocationUrl::class, 'raw_location_id');
    }

}
