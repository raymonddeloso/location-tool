<?php
use App\GoogleLocationResponse;
function locationType($location)
{
    if ($location->is_sublocality) {
        if (is_null($location->google_sublocality_level_2_long)) {
            return "Sublocality Level 1";
        }
        return "Sublocality Level 2";
    }

    if ($location->is_locality) {
        return "Locality";
    }

    if ($location->is_admin_area) {

        if (is_null($location->google_admin_area_level_3_long) && is_null($location->google_admin_area_level_2_long)) {
            return "Admin Area Level 1";
        }

        if (is_null($location->google_admin_area_level_3_long) && !is_null($location->google_sublocality_level_2_long)) {
            return "Admin Area Level 2";
        }

        if (!is_null($location->google_admin_area_level_3_long)) {
            return "Admin Area Level 3";
        }
    }
}

function getUnexplainedLocationResult($location)
{

    return GoogleLocationResponse::where('indeed_location_name_old', '=', $location)->groupBy('google_location_place_id')->get();

}

function calculateHaversine($pointA, $latitude2, $longitude2)
{
    $latitude1  = explode(",", $pointA)[0];
    $longitude1 = explode(",", $pointA)[1];

    $earth_radius = 6371;

    $dLat = deg2rad($latitude2 - $latitude1);
    $dLon = deg2rad($longitude2 - $longitude1);

    $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
    $c = 2 * asin(sqrt($a));
    $d = $earth_radius * $c;

    return $d;
}

function extractZip($string)
{
    if (preg_match('/^[0-9]+\s/', $string)) {
        $zip = explode(" ", $string);
        return $zip[0];
    }

}
