<?php

namespace App\Http\Controllers;

use App\GoogleLocationResponse;
use Illuminate\Http\Request;

class GoogleLocationResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GoogleLocationResponse  $googleLocationResponse
     * @return \Illuminate\Http\Response
     */
    public function show(GoogleLocationResponse $googleLocationResponse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GoogleLocationResponse  $googleLocationResponse
     * @return \Illuminate\Http\Response
     */
    public function edit(GoogleLocationResponse $googleLocationResponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GoogleLocationResponse  $googleLocationResponse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GoogleLocationResponse $googleLocationResponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GoogleLocationResponse  $googleLocationResponse
     * @return \Illuminate\Http\Response
     */
    public function destroy(GoogleLocationResponse $googleLocationResponse)
    {
        //
    }
}
