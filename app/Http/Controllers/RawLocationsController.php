<?php

namespace App\Http\Controllers;

use App\GoogleLocationResponse;
use App\log;
use App\NewLocationsLog;
use App\RawLocation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RawLocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RawLocation::select('raw_locations.id as raw_id', 'raw_locations.*', 'google_location_responses.*')
            ->join('google_location_responses', 'google_location_responses.id', '=', 'raw_locations.gg_loc_equivalent')
            ->orderBy('indeed_location_name')
            ->paginate(15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RawLocation  $rawLocation
     * @return \Illuminate\Http\Response
     */
    public function show(RawLocation $rawLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RawLocation  $rawLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(RawLocation $rawLocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RawLocation  $rawLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RawLocation $rawLocation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RawLocation  $rawLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(RawLocation $rawLocation)
    {
        //
    }

    /**
     * [showExplainedLocation description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function showExplainedLocation(Request $request)
    {

        /*$locations = RawLocation::select('raw_locations.id as raw_id', 'raw_locations.*', 'google_location_responses.*')
        ->join('google_location_responses', 'google_location_responses.id', '=', 'raw_locations.gg_loc_equivalent')
        ->orderBy('indeed_location_name')
        ->paginate(10);

        if ($request->ajax()) {
        return view('searchview', compact('locations'))->render();
        }*/

        return view('explained_location');
    }

    /**
     * [showUnexplainedLocation description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function showUnexplainedLocation(Request $request)
    {
        return RawLocation::where('is_explained', 0)
            ->where('undecided', 0)
            ->where('no_result', 0)
            ->orderBy('job_count', 'DESC')
            ->paginate(10);

        $sort  = 'job_count';
        $order = 'desc';

        if ($request->sort == "job_count") {
            $order = $request->order;
        }

        if ($request->sort == "location_name") {
            $sort  = "indeed_location_name";
            $order = $request->order;
        }

        $locations = RawLocation::with('hasManyUrl')->where('is_explained', 0)
            ->where('undecided', 0)
            ->where('no_result', 0)
            ->orderBy($sort, $order)
            ->paginate(10);

        if ($request->sort == "job_count") {
            $locations->setPath('/unexplained-location?sort=' . $sort . '&order=' . $order);
        }

        if ($request->sort == "location_name") {
            $locations->setPath('/unexplained-location?sort=' . $request->sort . '&order=' . $order);
        }

        return view('unexplained_location', compact('locations'));
    }

    /**
     * [showUndecidedLocation description]
     * @return [type] [description]
     */
    public function showUndecidedLocation()
    {
        $locations = RawLocation::with('hasManyUrl')
            ->where('undecided', 1)
            ->where('no_result', 0)->paginate(10);

        return view('undecided_location', compact('locations'));
    }

    /**
     * [showNoResult description]
     * @param  Request $request [descriptio
     * @return [type]           [description]
     */
    public function showNoResult(Request $request)
    {

        $sort  = 'job_count';
        $order = 'desc';

        if ($request->sort == "job_count") {
            $order = $request->order;
        }

        if ($request->sort == "location_name") {
            $sort  = "indeed_location_name";
            $order = $request->order;
        }

        $locations = RawLocation::where('no_result', 1)
            ->orderBy($sort, $order)
            ->paginate(10);

        if ($request->sort == "job_count") {
            $locations->setPath('/no-result?sort=' . $sort . '&order=' . $order);
        }

        if ($request->sort == "location_name") {
            $locations->setPath('/no-result?sort=' . $request->sort . '&order=' . $order);
        }

        return view('no_result', compact('locations'));
    }

    /**
     * [removeExplained description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function removeExplained(Request $request)
    {
        $rawLoc = RawLocation::find($request->id);

        $rawLoc->is_explained      = 0;
        $rawLoc->gg_loc_equivalent = null;
        $rawLoc->save();

        return $rawLoc;

    }

    /**
     * [explainLoc description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function explainLoc(Request $request)
    {

        $loc = RawLocation::find($request->donut_id);

        $loc->is_explained      = 1;
        $loc->gg_loc_equivalent = $request->gg_id;
        $loc->save();

        return $loc;
    }

    public function decideLoc(Request $request)
    {

        $loc = RawLocation::find($request->donut_id);

        $loc->undecided         = 0;
        $loc->is_explained      = 1;
        $loc->gg_loc_equivalent = $request->gg_id;
        $loc->save();

        return $loc;

    }

    public function undecideLoc(Request $request)
    {
        $loc = RawLocation::find($request->id);

        $loc->is_explained      = 0;
        $loc->gg_loc_equivalent = null;
        $loc->undecided         = 1;
        $loc->save();

        return $loc;
    }

    public function searchExplained(Request $request)
    {

        return RawLocation::select('raw_locations.id as raw_id', 'raw_locations.*', 'google_location_responses.*')
            ->join('google_location_responses', 'google_location_responses.id', '=', 'raw_locations.gg_loc_equivalent')
            ->where('indeed_location_name', 'like', $request->term . '%')
            ->orderBy('indeed_location_name')
            ->paginate(15)->appends(['term' => $request->term]);

    }

    public function showCrawlingLogs()
    {
        $locationlogs = NewLocationsLog::paginate(10);

        $rawLocationModel = new RawLocation;

        return view('crawling-log', compact('locationlogs', 'rawLocationModel'));
    }

    public function showCrawlingLogsByDate(Request $request)
    {

        $byDateLogs = RawLocation::where('batch_num', $request->date)->get();

        $byDateLogsExplained = RawLocation::select('raw_locations.id as raw_id', 'raw_locations.*', 'google_location_responses.*')
            ->where('raw_locations.batch_num', $request->date)
            ->where('raw_locations.is_explained', 1)
            ->join('google_location_responses', 'google_location_responses.id', '=', 'raw_locations.gg_loc_equivalent')
            ->orderBy('indeed_location_name')
            ->paginate(10);

        $byDateLogsExplained->setPath('/api/crawl-logs/explained/' . $request->date);

        $byDateLogsUnexplained = RawLocation::with('hasManyUrl')->where('is_explained', 0)
            ->where('batch_num', $request->date)
            ->where('undecided', 0)
            ->where('no_result', 0)
            ->orderBy('job_count', 'desc')
            ->paginate(10);

        $byDateLogsUnexplained->setPath('/api/crawl-logs/unexplained/' . $request->date);

        if ($byDateLogs->count() == 0) {
            echo "Page not Found";
        }

        return view('crawling-log-by-date', compact('byDateLogs', 'byDateLogsUnexplained', 'byDateLogsExplained'));

    }

    public function showErrorLogs()
    {
        $logs = \App\Log::paginate(10);
        return view('error-logs', compact('logs'));
    }

    public function getGoogleAddresses(Request $request)
    {
        $data = getUnexplainedLocationResult($request->location);
        return $data->chunk(3);
    }

    public function getJobKeys(Request $request)
    {
        $coordinates = explode(",", $request->coordinates);
        $jobkeys     = \DB::connection('crawler_server')
            ->table('indeed_jobs')
            ->select('jobkey', 'title', 'description', 'company')
            ->where('city', $request->city)
            ->where('lat', $coordinates[0])
            ->where('lon', $coordinates[1])
            ->where('date_tagged', '>=', Carbon::now()->subDays(60)->toDateString())
            ->groupBy('jobkey')
            ->take(30)
            ->get();

        return $jobkeys;
    }

    public function getSublocalities()
    {
        $locs = \DB::Table('raw_locations')
            ->join('google_location_responses', 'raw_locations.gg_loc_equivalent', '=', 'google_location_responses.id')
            ->select('raw_locations.indeed_coordinates', 'raw_locations.indeed_location_name', 'raw_locations.job_count', 'google_location_responses.google_location_name',
                'google_location_responses.google_sublocality_level_1_long', 'google_location_responses.google_sublocality_level_2_long',
                'google_location_responses.google_locality_long', 'google_location_responses.google_location_lat', 'google_location_responses.google_location_lng')
            ->where('google_location_responses.is_sublocality', 1)
            ->where('raw_locations.is_explained', 1)
            ->paginate(10);
        return view('sublocalities', compact('locs'));

    }
}
