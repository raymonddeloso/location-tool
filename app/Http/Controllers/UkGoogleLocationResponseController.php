<?php

namespace App\Http\Controllers;

use App\UkGoogleLocationResponse;
use Illuminate\Http\Request;

class UkGoogleLocationResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UkGoogleLocationResponse  $ukGoogleLocationResponse
     * @return \Illuminate\Http\Response
     */
    public function show(UkGoogleLocationResponse $ukGoogleLocationResponse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UkGoogleLocationResponse  $ukGoogleLocationResponse
     * @return \Illuminate\Http\Response
     */
    public function edit(UkGoogleLocationResponse $ukGoogleLocationResponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UkGoogleLocationResponse  $ukGoogleLocationResponse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UkGoogleLocationResponse $ukGoogleLocationResponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UkGoogleLocationResponse  $ukGoogleLocationResponse
     * @return \Illuminate\Http\Response
     */
    public function destroy(UkGoogleLocationResponse $ukGoogleLocationResponse)
    {
        //
    }
}
