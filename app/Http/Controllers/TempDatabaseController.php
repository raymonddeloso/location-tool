<?php

namespace App\Http\Controllers;

use App\TempDatabase;
use Illuminate\Http\Request;

class TempDatabaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TempDatabase  $tempDatabase
     * @return \Illuminate\Http\Response
     */
    public function show(TempDatabase $tempDatabase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TempDatabase  $tempDatabase
     * @return \Illuminate\Http\Response
     */
    public function edit(TempDatabase $tempDatabase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TempDatabase  $tempDatabase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TempDatabase $tempDatabase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TempDatabase  $tempDatabase
     * @return \Illuminate\Http\Response
     */
    public function destroy(TempDatabase $tempDatabase)
    {
        //
    }
}
