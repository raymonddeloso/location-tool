<?php

namespace App\Http\Controllers;

use App\TmpGoogleResponse;
use Illuminate\Http\Request;

class TmpGoogleResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TmpGoogleResponse  $tmpGoogleResponse
     * @return \Illuminate\Http\Response
     */
    public function show(TmpGoogleResponse $tmpGoogleResponse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TmpGoogleResponse  $tmpGoogleResponse
     * @return \Illuminate\Http\Response
     */
    public function edit(TmpGoogleResponse $tmpGoogleResponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TmpGoogleResponse  $tmpGoogleResponse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TmpGoogleResponse $tmpGoogleResponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TmpGoogleResponse  $tmpGoogleResponse
     * @return \Illuminate\Http\Response
     */
    public function destroy(TmpGoogleResponse $tmpGoogleResponse)
    {
        //
    }
}
