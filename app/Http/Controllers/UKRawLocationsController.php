<?php

namespace App\Http\Controllers;

use App\UkGoogleLocationResponse;
use App\UkRawLocation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UKRawLocationsController extends Controller
{
    public function showExplainedView()
    {
        return view('uk.explained');
    }

    public function showUnexplainedView()
    {
        return view('uk.unexplained');
    }

    public function showUndecidedView()
    {
        return view('uk.undecided');
    }

    public function showNotCrawledView()
    {
        return view('uk.notcrawled');
    }

    public function index()
    {
        return UkRawLocation::select('uk_raw_locations_copy.id as raw_id', 'uk_raw_locations_copy.*', 'uk_google_location_responses.*')
            ->join('uk_google_location_responses', 'uk_google_location_responses.id', '=', 'uk_raw_locations_copy.gg_loc_equivalent')
            ->where('is_explained', 1)
            ->orderBy('indeed_location_name')
            ->paginate(15);
    }

    public function unexplainedIndex()
    {
        return UkRawLocation::select('uk_raw_locations_copy.id as raw_id', 'uk_raw_locations_copy.*')
            ->where('is_explained', 0)
            ->where('no_result', 0)
            ->where('undecided', 0)
            ->orderBy('job_count', 'desc')
            ->paginate(15);
    }

    public function search(Request $request)
    {
        return UkRawLocation::select('uk_raw_locations_copy.id as raw_id', 'uk_raw_locations_copy.*', 'uk_google_location_responses.*')
            ->join('uk_google_location_responses', 'uk_google_location_responses.id', '=', 'uk_raw_locations_copy.gg_loc_equivalent')
            ->where('indeed_location_name', 'LIKE', $request->term . '%')
            ->where('is_explained', 1)
            ->orderBy('indeed_location_name')
            ->paginate(15)->appends(['term' => $request->term]);
    }

    public function searchUnexplained(Request $request)
    {
        return UkRawLocation::select('uk_raw_locations_copy.id as raw_id', 'uk_raw_locations_copy.*')
            ->where('indeed_location_name', 'LIKE', $request->term . '%')
            ->where('is_explained', 0)
            ->where('no_result', 0)
            ->where('undecided', 0)
            ->orderBy('job_count')
            ->paginate(15)->appends(['term' => $request->term]);
    }

    public function getJobKeys(Request $request)
    {
        $coordinates = explode(",", $request->coordinates);

        $jobkeys = \DB::connection('crawler_server')
            ->table('indeed_jobs')
            ->select('jobkey', 'title', 'description', 'company', 'city')
            ->where('city', $request->city)
            ->where('lat', $coordinates[0])
            ->where('lon', $coordinates[1])
            ->where('date_tagged', '>=', Carbon::now()->subDays(60)->toDateString())
            ->groupBy('jobkey')
            ->take(30)
            ->get();

        return $jobkeys;
    }

    public function googleLocations(Request $request)
    {
        return UkGoogleLocationResponse::where('indeed_location_name_old', $request->location)
            ->get()->chunk(3);
    }

    public function explainLoc(Request $request)
    {

        $loc = UkRawLocation::find($request->id);

        $loc->is_explained      = 1;
        $loc->undecided         = 0;
        $loc->gg_loc_equivalent = $request->gg_id;
        $loc->save();

    }

    public function undecideLoc(Request $request)
    {
        $loc = UkRawLocation::find($request->id);

        $loc->is_explained      = 0;
        $loc->gg_loc_equivalent = null;
        $loc->undecided         = 1;
        $loc->save();

        return $loc;
    }

    public function removeExplained(Request $request)
    {
        $rawLoc = UkRawLocation::find($request->id);

        $rawLoc->is_explained      = 0;
        $rawLoc->gg_loc_equivalent = null;
        $rawLoc->save();

        return $rawLoc;

    }

    public function undecidedIndex()
    {
        return UkRawLocation::where('undecided', 1)
            ->orderBy('job_count', 'DESC')
            ->paginate(15);
    }

    public function notCrawled()
    {
        return \DB::table('uk_location_matches')
            ->where('match_type', 'NOT_CRAWLED')
            ->orderBy('job_count', 'DESC')
            ->paginate(15);
    }
}
