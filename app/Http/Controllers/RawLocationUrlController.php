<?php

namespace App\Http\Controllers;

use App\RawLocationUrl;
use Illuminate\Http\Request;

class RawLocationUrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RawLocationUrl  $rawLocationUrl
     * @return \Illuminate\Http\Response
     */
    public function show(RawLocationUrl $rawLocationUrl)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RawLocationUrl  $rawLocationUrl
     * @return \Illuminate\Http\Response
     */
    public function edit(RawLocationUrl $rawLocationUrl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RawLocationUrl  $rawLocationUrl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RawLocationUrl $rawLocationUrl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RawLocationUrl  $rawLocationUrl
     * @return \Illuminate\Http\Response
     */
    public function destroy(RawLocationUrl $rawLocationUrl)
    {
        //
    }
}
