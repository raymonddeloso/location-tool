<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewLocationsLog extends Model
{
    protected $table = "new_locations_logs";
}
