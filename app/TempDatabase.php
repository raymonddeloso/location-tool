<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempDatabase extends Model
{
    protected $table = "temp_databases";
}
