<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RawLocationUrl extends Model
{
    protected $table = "raw_location_urls";
}
